# LCM Unity MATLAB

# Build Instructions 

## MAC OS

### Dependencies
```bash
brew update
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk8
brew cask install adoptopenjdk8-jre
brew install glib pkg-config
brew install cmake
```

### Install LCM
```bash
git clone git@bitbucket.org:Farnie/lcm-fork.git ~/git/lcm
cd ~/git/lcm
mkdir build
cd build
cmake ..
make
sudo make install
```

### Clone lcm-unity-matlab
```bash
git clone git@bitbucket.org:Farnie/lcm-unity-matlab.git ~/lcm-unity-matlab
```

#### Build message types

```bash
cd ~/lcm-unity-matlab/lcm-messages
./build_msg.sh
```

1. Copy the contents from `scripts-for-unity` to your unity `Assests/Scripts` folder.
2. Attach the `raycast.cs` script to your camera game object.
3. Run your game.
4. Open Matlab and run `examples/runUnityRender.m` to render your scene and associated depth field.