
profile off


restoredefaultpath
addpath lcm-message-types/
addpath lcm-interface/
addpath lcm-java/
addpath general/
addpath lens/
addpath opticalflow/

clc
clear all
pause on % to enable pause function

save = true;
n = 10;
rCNn	= [ 1; % North 
			0; % East
			0];% Down

% Camera rotation matrix. 
% Rnc = [c1n, c2n, c3n]. Where c1n, c2n, c3n \in \mathbb{R}^{3} : 
% The camera points in the direction c1n
Rnc         = eye(3); 		
fps         = 30;
vCNc        = [1; 0;0;];
omegaBNc    = [0; 0; 0];
frames      = {};
rhos        = {};
projs       = {};
rQCcs       = {};
for i=1:n
    [rho, frame, rQCc, proj] = renderUnity(rCNn, Rnc);
    frames{i} = frame;
    projs{i} = proj;
    rhos{i} = rho;
    rQCcs{i} = rQCc;
    pause(0.01)
end

if save
    for i=1:n        
        imwrite(frames{i}, strcat("frame", num2str(i), ".png"));
        if i == 1
            continue
        end
        [nv, nu, nc]	= size(frames{i-1});
        Kc				= generateKc(projs{i-1}, nu, nv);
        param.rQCc		= rQCcs{i-1};
        param.v2p		= @(v)v2p(v, Kc);
        param.p2v		= @(p)p2v(p, Kc);
        % Ray terminations (P) with respect to the camera origin (C) described in 
        % basis {c}

        rPCc			= rQCcs{i-1}./rhos{i-1};
        % Ray terminations (P) with respect to the world fixed origin (N) described in 
        % NED basis {n}
        rPNn			= Rnc*rPCc + rCNn;

        % TODO: Generate a smooth trajectory with associated vCNc, omegaBNc, Rnb
        % and rCNn

        % geometric flow calculation.    
        BdrQCc				= geometricOF(vCNc, omegaBNc, rhos{i}, rQCcs{i-1});

        % ground truth image flow for the given inverse depth [rho] and body fixed
        % velocities
        imflow				= sphere2imflow(BdrQCc, param);
        renderflow		= imflow/fps;
        flow = reshape(renderflow, [nv, nu, 2]);
        
        write_flow(flow, strcat("flow",num2str(i-1),".flo"));
    end
end
profile viewer
