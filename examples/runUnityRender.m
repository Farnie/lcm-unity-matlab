clc
clear all

restoredefaultpath
addpath	../lcm-interface/
addpath	../lcm-interface/c/
addpath	../lcm-java/
addpath	../lcm-message-types/
addpath ../general/
addpath ../lens/
addpath ../opticalflow/



save = true;

rCNn	= [ -217; % North 
			191; % East
			-345];% Down

% Camera rotation matrix. 
% Rnc = [c1n, c2n, c3n]. Where c1n, c2n, c3n \in \mathbb{R}^{3} : 
% The camera points in the direction c1n
Rnc         = eye(3); 		
fps         = 30;


fontsize	= 18;


figure(1);clf;
subplot(2,2,[2,4]);
h_ptcld		= plot3(0,0,0,'.');hold on
h_ptcld.Color=[0.35*[1,1,1],0.1];

s           = 50;

c1n 		= Rnc(:,1);
h_basis1    = quiver3(rCNn(1), rCNn(2), rCNn(3), s*c1n(1), s*c1n(2), s*c1n(3), 0, 'displayname', '$\mathbf{c}_{1}^{n}$');
c2n 		= Rnc(:,2);
h_basis2    = quiver3(rCNn(1), rCNn(2), rCNn(3), s*c2n(1), s*c2n(2), s*c2n(3), 0, 'displayname', '$\mathbf{c}_{2}^{n}$');
c3n 		= Rnc(:,3);
h_basis3    = quiver3(rCNn(1), rCNn(2), rCNn(3), s*c3n(1), s*c3n(2), s*c3n(3), 0, 'displayname', '$\mathbf{c}_{3}^{n}$');
set(h_basis1,'linewidth',3);
set(h_basis2,'linewidth',2);
set(h_basis3,'linewidth',2);



daspect([1,1,1]);
xlabel('N - [m]','Interpreter','latex','FontSize', fontsize);
ylabel('E - [m]','Interpreter','latex','FontSize', fontsize);
zlabel('D - [m]','Interpreter','latex','FontSize', fontsize);
set(gca,'ydir','reverse');
set(gca,'zdir','reverse');
legend([h_basis1, h_basis2, h_basis3],'Interpreter','latex','FontSize', fontsize);


[rho, frame1, rQCc, proj] = renderUnity(rCNn, Rnc);
[nv, nu, nc]	= size(frame1);
Kc				= generateKc(proj, nu, nv);
param.rQCc		= rQCc;
param.v2p		= @(v)v2p(v, Kc);
param.p2v		= @(p)p2v(p, Kc);

u				= 1:nu;
v				= 1:nv;
[U,V]			= meshgrid(u,v);
p				= [U(:),V(:)]';
qimf			= 5; % Set plot quantisation
qimf_idxOI		= (1:qimf:nv)' + nv*((1:qimf:nu)-1);
qimf_idxOI		= qimf_idxOI(:);
	

subplot(2,2,1);
h_image			= image(zeros(nv, nu, nc)); 
daspect([1,1,1]);
hold on;
h_qimf			= quiver(p(1,qimf_idxOI),p(2,qimf_idxOI),0.*p(2,qimf_idxOI),0.*p(2,qimf_idxOI),0);

% Ray terminations (P) with respect to the camera origin (C) described in 
% basis {c}
% If points are further than <max_range> metres away, assume they hit the sky.
max_range       = 100;

subplot(2,2,3);
h_dimage = imagesc(zeros(nv, nu));

daspect([1,1,1]);


T = 50;
profile on 
for t = 1:T
    rCNnt               = rCNn + [0;0;-10]*t/T;
    [rho, frame2]		= renderUnity(rCNnt, Rnc, false);
    
    rho((1./rho)>max_range)=0;
    
    depthimg        = 1./rho;
    range           = 1./rho(:)';
    
    rPCc			= rQCc.*range;
    % Ray terminations (P) with respect to the world fixed origin (N) described in 
    % NED basis {n}
    rPNn			= Rnc*rPCc + rCNn;


    set(h_ptcld, 'xdata', rPNn(1,:), 'ydata', rPNn(2,:), 'zdata', rPNn(3,:));
    set(h_dimage, 'cdata', rho);
    set(h_image,'cdata',frame2)
    drawnow;
end

profile viewer





