function b = mm3d(varargin)
%MM3D Matrix multiply for 3D matrices
% 
   
    b = mm3d_vecpage(varargin{:});
end

function b = mm3d_vecpage(A, x, trans)
%MM3D Matrix multiply for 3D matrices
% b(:,i) = A(:,:,i)*x(:,i);
	
    
    if(nargin==2)
        trans = 'N';
    end
    
    if(trans == 'N')
        
        dim_Aa      = size(A,1);
        dim_Ac      = size(A,2);
    else
        dim_Aa      = size(A,2);
        dim_Ac      = size(A,1);
    end
    dim_Ap   	= size(A,3);

    dim_xc      = size(x,1);
    dim_xb      = size(x,2);
    dim_xp      = size(x,3);

    dim_ba      = dim_Aa;
    dim_bb      = dim_xb;
    dim_bp      = max(dim_xp, dim_Ap);

    b       = zeros(dim_ba, dim_bb, dim_bp);

    assert(dim_Ac == dim_xc,'Incorrect dimensions');

    for i = 1:dim_Aa
        if(trans == 'N')
            Ar			= A(i,:,:);        
        else
            Ar			= A(:,i,:);  
        end
        
        Ar			= reshape(Ar, dim_Ac, dim_Ap);
        for j = 1:dim_xb
            xc            = squeeze(x(:,j,:));
            b(i,j,:)      = sum(Ar.*xc, 1);
        end
    end
end