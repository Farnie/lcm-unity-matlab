function flow = read_flow(filename)
%READ_FLOW Summary of this function goes here
%   Detailed explanation goes here
assert(isa(filename, 'string'),'Filename is not type string.');
assert(filename ~= "", 'Filename is empty')
filename_char = char(filename);
assert(all((filename_char(end-3:end) == '.flo')), 'Filename does not end with .flo');

fid = fopen(filename, 'r');
assert(fid > 0);
tag = fread(fid, 1, 'float32');
w = fread(fid, 1, 'int32');
h = fread(fid, 1, 'int32');

assert(tag == 202021.25);
assert(w > 1 && w < 99999);
assert(h > 1 && h < 99999);

n = 2;
tmp = fread(fid, inf, 'float32');
tmp = reshape(tmp, [w*n, h]);
tmp = tmp';
flow(:,:,1) =tmp(:,(1:w)*n -1);
flow(:,:,2) = tmp(:, (1:w)*n);
fclose(fid);
end

