function [Theta_q] = rot2quat(Rnb)
    
    sign = @(x)(1 - 2*(x<0));
    
    cps = @(x,y) (abs(x) * sign(y));
    s = 0.5*sqrt(1 + Rnb(1,1) + Rnb(2,2) + Rnb(3,3));
    x = cps(0.5*sqrt(1 + Rnb(1,1) - Rnb(2,2) - Rnb(3,3)), Rnb(3,2) - Rnb(2,3));
    y = cps(0.5*sqrt(1 - Rnb(1,1) + Rnb(2,2) - Rnb(3,3)), Rnb(1,3) - Rnb(3,1));
    z = cps(0.5*sqrt(1 - Rnb(1,1) - Rnb(2,2) + Rnb(3,3)), Rnb(2,1) - Rnb(1,2));
    
    Theta_q = [s;x;y;z];
end

