function [R, R_MCHA_3900] = rot_eulerAng(vec)
    % Generates the rotation matrix for euler angles. Taken from page 34 of
    % the MCHA3900 lecture notes. RPY Euler Angles
    % Roll -> Pitch -> Yaw
    r_s = @(x)(sin(x));
    r_cc = @(x,y)(cos(x)*cos(y));
    r_cs = @(x,y)(cos(x)*sin(y));
    r_sc = @(x,y)(r_cs(y,x));
    r_ss = @(x,y)(sin(x)*sin(y));
    r_css = @(x,y,z)(cos(x)*sin(y)*sin(z));
    r_ccs = @(x,y,z)(cos(x)*cos(y)*sin(z));
    r_sss = @(x,y,z)(sin(x)*sin(y)*sin(z));
    r_scs = @(x,y,z)(sin(x)*cos(y)*sin(z));

    if(length(vec)~=3)
        error('Expecting vec to be of length 3');
    end
    phi     = vec(1);
    theta   = vec(2);
    psi     = vec(3);
    
    RPY=[cos(psi)*cos(theta), cos(psi)*sin(phi)*sin(theta) - cos(phi)*sin(psi), sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta);
         cos(theta)*sin(psi), cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta), cos(phi)*sin(psi)*sin(theta) - cos(psi)*sin(phi);
                 -sin(theta),                              cos(theta)*sin(phi),                              cos(phi)*cos(theta)];
 
    R_MCHA_3900 = [cos(phi)*cos(theta), cos(psi)*sin(theta)*sin(phi) - sin(phi)*cos(psi), sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta);
                   cos(theta)*sin(phi), cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta), cos(psi)*sin(phi)*sin(theta) - cos(phi)*sin(psi);
                    -sin(theta),                                     cos(theta)*sin(psi),                              cos(psi)*cos(theta)];
 
    R = RPY;


end