function Rnb = rot_q(Theta_q)

    s = Theta_q(1);
    x = Theta_q(2);
    y = Theta_q(3);
    z = Theta_q(4);
    
    Rnb     = [ 1 - 2*y*y-2*z*z,    2*x*y - 2*z*s,      2*x*z + 2*y*s;
                2*x*y + 2*z*s,      1-2*x*x-2*z*z,      2*y*z - 2*x*s;
                2*x*z - 2*y*s,      2*y*z + 2*x*s,      1 - 2*x*x - 2*y*y];




end