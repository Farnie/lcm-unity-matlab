function write_flow(flow,filename)
    %WRITE_FLOW Summary of this function goes here
    %   Writes a flow vector to file.
    assert(isa(filename, 'string'),'Filename is not type string.');
    filename_char = char(filename);
    assert(all((filename_char(end-3:end) == '.flo')), 'Filename does not end with .flo');

    [h, w, n] = size(flow);

    assert(n == 2)

    u = flow(:,:,1);
    v = flow(:,:,2);

    assert(all(size(u) == size(v)))

    fid = fopen(filename, 'w');
    assert(fid > 0);

    % write the header
    fwrite(fid, 'PIEH'); 
    fwrite(fid, w, 'int32');
    fwrite(fid, h, 'int32');

    % arrange into matrix form
    tmp = zeros(h, w*n);

    tmp(:, (1:w)*n-1) = flow(:,:,1);
    tmp(:, (1:w)*n) = squeeze(flow(:,:,2));
    tmp = tmp';

    fwrite(fid, tmp, 'float32');

    fclose(fid);
end

