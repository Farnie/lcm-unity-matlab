clc
clear

file            = "getLens";

delete("bin/*" + file + "*");

target          = [dir("src/mex_"+ file + ".c"); dir("src/sub_"+ file + ".c")];
make(target);
%%
msg.rCNu        = [200,400,-217]';
msg.Qur         = [1; zeros(3,1)];
msg.runRender   = 0; % Send lens
msg.doGeoDepth  = 1;
msg.id          = 0;
msg.pkt_initID  = 0;


addpath ./bin
[rQCr1, rQCr2, rQCr3, proj] = mex_getLens(msg);
restoredefaultpath

% 
% imagev  = image(:);
% nv      = size(image,1);
% nu      = size(image,2);
% nc      = size(image,3);
% 
% np      = nu*nv;
% 
% idx     = 3*((np:-1:1) - 1);
% idx_r   = idx + 1;
% idx_g   = idx + 2;
% idx_b   = idx + 3;
% 
% image_r = zeros(nv, nu)';
% image_g = zeros(nv, nu)';
% image_b = zeros(nv, nu)';
% 
% image_r(:) = imagev(idx_r);
% image_g(:) = imagev(idx_g);
% image_b(:) = imagev(idx_b);
% 
% imagen     = uint8(cat(3, image_r', image_g', image_b'));


% figure(1);clf;
% imshow(image,'initialmagnification','fit');