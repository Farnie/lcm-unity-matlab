function [rQCr,proj] = getLens()
%GETLENS Summary of this function goes here
%   Detailed explanation goes here
	msg.rCNu        = [200,400,-217]';
    msg.Qur         = [1; zeros(3,1)];
    msg.runRender   = 0; % Send lens
    msg.doGeoDepth  = 1;
    msg.id          = 0;

    [rQCr1, rQCr2, rQCr3, proj] = mex_getLens(msg);
    rQCr            = double([rQCr1';rQCr2';rQCr3']);
    proj            = double(proj);
   
    
end

