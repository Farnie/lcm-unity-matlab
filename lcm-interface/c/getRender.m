function [rho,im] = getRender(msg)
%GETRENDER Summary of this function goes here
%   Detailed explanation goes here
	[rho, image] = mex_getRender(msg);
    

    if(sum(abs(rho(:)))<1e-6)
        warning('No depth info');
    end
    
    nv          = size(image,1);
    nu          = size(image,2);
    nc          = size(image,3);
    np 			= nv*nu;
	pselect		= 3*((np:-1:1)-1);
	ridx		= pselect + 1;
	bidx		= pselect + 2;
	gidx		= pselect + 3;
	r 			= image(ridx);
	g 			= image(bidx);
	b 			= image(gidx);
	im(:,:,1)	= reshape(r, nu, nv)';
	im(:,:,2)	= reshape(g, nu, nv)';
	im(:,:,3) 	= reshape(b, nu, nv)';
	im			= im(:,end:-1:1,:);
    
    % inverse depth
    if(msg.doGeoDepth == 0)
        zrho_gpu      	= zeros(nu,nv);
        zrho_gpu(:)   	= rho(:);
        zrho_gpu      	= zrho_gpu';
        zrho_gpu    	= zrho_gpu(end:-1:1,:);
        rho             = zrho_gpu;
    else
        rho             = reshape(rho, nv, nu);
    end
    rho             = double(rho);
end

