// file: listener.c
//
// LCM example program.
//
// compile with:
//  $ gcc -o listener listener.c -llcm
//
// On a system with pkg-config, you can also use:
//  $ gcc -o listener listener.c `pkg-config --cflags --libs lcm`

#include <inttypes.h>
#include <lcm/lcm.h>
#include <stdio.h>
#include "../unity/unity_ctl_t.h"

static void my_handler(const lcm_recv_buf_t *rbuf, const char *channel, const unity_ctl_t *msg,
                       void *user)
{
    int i;
    printf("Received message on channel \"%s\":\n", channel);
    printf("  rCNu          = (%f, %f, %f)\n", msg->rCNu[0], msg->rCNu[1], msg->rCNu[2]);
    printf("  Qur           = (%f, %f, %f, %f)\n", msg->Qur[0], msg->Qur[1],
           msg->Qur[2], msg->Qur[3]);
    printf("  runRender     = %d\n", msg->runRender);
    printf("  doGeoDepth    = %d\n", msg->doGeoDepth);
    printf("  id            = %d\n", msg->id);
    printf("  pkt_initID    = %d\n", msg->pkt_initID);
}

int main(int argc, char **argv)
{
    lcm_t *lcm = lcm_create(NULL);
    if (!lcm)
        return 1;

    unity_ctl_t_subscribe(lcm, "UNITY_CTL", &my_handler, NULL);

    while (1)
        lcm_handle(lcm);

    lcm_destroy(lcm);
    return 0;
}
