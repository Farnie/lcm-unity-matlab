function T = make(target)

if(nargin==0)
    file        = "listener";
    target      = [dir("src/mex_"+ file + ".c"); dir("src/sub_"+ file + ".c")];
end

sourceUnity = dir('unity/*.c');
sourceMath  = dir('embed_math_lib/*.c');
sourceAll   = [target; sourceUnity;sourceMath];

assert(isempty(target)==0,'File(s) do not exist');
% Files to compile
sourceFiles     = string;
for i = 1:length(sourceAll)
    srcfi   = sourceAll(i).folder + string(filesep) + sourceAll(i).name; 
    sourceFiles = sourceFiles + " " + srcfi;
end

% Run these macros
% CFLAGS      = '''pkg-config --cflags lcm''';
% LDFLAGS     = '''pkg-config --libs lcm''';
[lcm_incflags, lcm_ldflags]   = pkg_config("lcm");
LDFLAGS     = lcm_ldflags;

COPTIMFLAGS = """-O2 -fwrapv""";

DEFINES     = "";
DEFINES     = DEFINES + " -D" + "INT_LONG_LONG_INT";

INCLUDES    = "";
INCLUDES    = INCLUDES + " -I""./unity""";
INCLUDES    = INCLUDES + " -I""./embed_math_lib""";
INCLUDES    = INCLUDES + " " + string(lcm_incflags);

pre         = "mex ";
pre         = pre + " " + "COPTIMFLAGS=" + COPTIMFLAGS;
pre         = pre + " " + string(DEFINES);
pre         = pre + " " + string(INCLUDES);
if(isunix)
    post        = " -lblas -llapack " + string(LDFLAGS);
else
    post        = " -lmwblas -lmwlapack " + string(LDFLAGS); 
end

mex_cstr    = pre + " " + sourceFiles + " " + post;

eval(mex_cstr);

moveBinaries;