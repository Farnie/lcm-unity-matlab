clc
clear

% Generate data types for the unity lcm messages
% !./gen-types.sh

file            = "getLens";

delete("bin/*" + file + "*");

target          = [dir("src/mex_"+ file + ".c"); dir("src/sub_"+ file + ".c")];
make(target);

file            = "getRender";

delete("bin/*" + file + "*");

target          = [dir("src/mex_"+ file + ".c"); dir("src/sub_"+ file + ".c")];
make(target);