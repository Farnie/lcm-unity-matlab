targetDir   = "./bin";
if(exist('./bin','dir')~=7)
    mkdir(targetDir);
end
extlist     = mexext('all');

nexts   = length(extlist);

for i = 1:nexts
    ext = string(extlist(i).ext);
    exp  = "*." + ext;
    
    f   = dir("*." + ext);
    nf  = length(f);
    if(nf)
        movefile(exp, targetDir);
    end
end