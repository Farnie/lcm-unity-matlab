function [rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur, doGeo)
%SETPOSEGETFRAME Summary of this function goes here
%   Detailed explanation goes here
	persistent lc;
	
	persistent id rQCrp projp nt;
		
	if(isempty(lc))
        % Setup multicast
        script_root       = string(fileparts(mfilename('fullpath')));
%         !.././init_multicast.sh
        system(fullfile(script_root, ".././init_multicast.sh"))
        
               
        addpath(fullfile(script_root, "bin"));
        nt              = 0;
        id              = checksum(rCNu,Qur);
        lc              = 1;
		[rQCr, proj]    = getLens();
		rQCrp			= rQCr;
		projp			= proj;
	end
	nt = nt + 1;
	
	if(nargin~=3)
		doGeo			= true;
	end
	msg.rCNu        = rCNu;
    msg.Qur         = Qur;
    msg.runRender   = 1; 
    msg.doGeoDepth  = doGeo;
    msg.id          = checksum(rCNu,Qur);
	% Post Message
	[rho,image] = getRender(msg);
    
    % Correction for ray angle
    if(doGeo==0)
        rho(:)      = rho(:)'.*rQCrp(3,:);
    end
	
% 	fprintf('Transaction %d complete \n', nt);

	rQCr		= rQCrp;
	proj		= projp;
end
