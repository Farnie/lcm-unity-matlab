#include "mex.h"
#include "matrix.h"

#include "embed_matrix.h"
#include "embed_mex.h"


#include "unity_ctl_t.h"
#include "sub_getLens.h"

#define MAXCHARS 80   /* max length of string contained in each field */

/* The gateway function 
nlhs: Number of output (left-side) arguments, or the size of the plhs array
plhs: Array of output arguments
nrhs: Number of input (right-side) arguments, or the size of the prhs array
prhs: Array of input arguments
*/
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{


    if (nrhs != 1){ // check for two inputs
        mexErrMsgIdAndTxt("getLens:inputs",
            "1 input required");
    }else if (!mxIsStruct(prhs[0])){
        uint32_t i = 0;
        i = 0; if(!mxIsStruct(prhs[i])) printf("input %d is not a structure\n", i+1);
            mexErrMsgIdAndTxt("getLens:inputs",
            "both input must be a structure");
        }

    tic();
    mxArray *tmp;
    /* ------- DECLARE AND GET unity_ctl_t -------------- */
    DEBUG_GET_LENS("Declaring ctl");
    tmp         = prhs[0];
    // Use rBOn by reference
    unity_ctl_t ctl;

    arrayFromField_f(&(ctl.Qur),  4, tmp, "msg", "Qur");
    arrayFromField_f(&(ctl.rCNu), 3, tmp, "msg", "rCNu");
    ctl.runRender       = fscalarFromField(tmp, "msg", "runRender");
    ctl.doGeoDepth      = fscalarFromField(tmp, "msg", "doGeoDepth");
    ctl.id              = fscalarFromField(tmp, "msg", "id");

    // Initialize LCM

    lcm_t *lcm      = lcm_create(NULL);
    if (!lcm){
        mexErrMsgIdAndTxt("getLens",
            "LCM could not initialise");
        return;
    }
    

    /*-------------- COMPUTATIONAL ROUTINE-------------------- */
    trans_t trans;
    trans_init(&trans);
    
    DEBUG_GET_LENS("Calling getLens");

    getLens(lcm, &trans, &ctl);
   
    DEBUG_GET_LENS("Returned getLens");

    /*----------- POST OUTPUTS-------------------- */
    
    mwSize dims[3];
    uint32_t nbytes;
    float * pt0;
    // Post direction vectors

    DEBUG_GET_LENS("Post rQCr1");
    dims[0]         = trans.nrays; 
    dims[1]         = 1; 
    dims[2]         = 1; 
    tmp             = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
    pt0             = mxGetPr(tmp);
    nbytes          = sizeof(float) * (trans.nrays);
    memcpy (pt0, trans.rQCr1, nbytes);

    plhs[0]         = tmp;

    DEBUG_GET_LENS("Post rQCr2");
    tmp             = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
    pt0             = mxGetPr(tmp);
    nbytes          = sizeof(float) * (trans.nrays);
    memcpy (pt0, trans.rQCr2, nbytes);

    plhs[1]         = tmp;

    DEBUG_GET_LENS("Post rQCr3");
    tmp             = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
    pt0             = mxGetPr(tmp);
    nbytes          = sizeof(float) * (trans.nrays);
    memcpy (pt0, trans.rQCr3, nbytes);

    plhs[2]         = tmp;
    
    // Projection matrix
    DEBUG_GET_LENS("Post projection matrix");
    dims[0]         = 4; 
    dims[1]         = 4; 
    dims[2]         = 1; 
    tmp             = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL);
    uint8_t * pt1   = mxGetPr(tmp);
    nbytes          = sizeof(float) * (trans.proj_numel);
    memcpy (pt1, trans.proj, nbytes);

    plhs[3]         = tmp;
    
    DEBUG_GET_LENS("Outputs posted");

    // Clean up
    DEBUG_GET_LENS("Free");
    lens_cleanup();
    lcm_destroy(lcm);

    
    DEBUG_GET_LENS("Exit");
    
}

 