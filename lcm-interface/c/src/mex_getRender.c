#include "mex.h"
#include "matrix.h"

#include "embed_matrix.h"
#include "embed_mex.h"

#include "unity_ctl_t.h"
#include "sub_getRender.h"

#define MAXCHARS 80   /* max length of string contained in each field */

/* The gateway function 
nlhs: Number of output (left-side) arguments, or the size of the plhs array
plhs: Array of output arguments
nrhs: Number of input (right-side) arguments, or the size of the prhs array
prhs: Array of input arguments
*/
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{


    if (nrhs != 1){ // check for two inputs
        mexErrMsgIdAndTxt("getRender:inputs",
            "1 input required");
    }else if (!mxIsStruct(prhs[0])){
        uint32_t i = 0;
        i = 0; if(!mxIsStruct(prhs[i])) printf("input %d is not a structure\n", i+1);
            mexErrMsgIdAndTxt("getRender:inputs",
            "both input must be a structure");
        }

    tic();
    mxArray *tmp;
    /* ------- DECLARE AND GET unity_ctl_t -------------- */
    DEBUG_GET_RENDER("Declaring ctl");
    tmp         = prhs[0];
    // Use rBOn by reference
    unity_ctl_t ctl;

    arrayFromField_f(&(ctl.Qur),  4, tmp, "msg", "Qur");
    arrayFromField_f(&(ctl.rCNu), 3, tmp, "msg", "rCNu");
    ctl.runRender       = fscalarFromField(tmp, "msg", "runRender");
    ctl.doGeoDepth      = fscalarFromField(tmp, "msg", "doGeoDepth");
    ctl.id              = fscalarFromField(tmp, "msg", "id");

    // Initialize LCM

    lcm_t *lcm      = lcm_create(NULL);
    if (!lcm){
        mexErrMsgIdAndTxt("getRender",
            "LCM could not initialise");
        return;
    }
    

    /*-------------- COMPUTATIONAL ROUTINE-------------------- */
    trans_t trans;
    trans_init(&trans);
    
    DEBUG_GET_RENDER("Calling getRender");

    getRender(lcm, &trans, &ctl);
   
    DEBUG_GET_RENDER("Returned getRender");

    /*----------- POST OUTPUTS-------------------- */
    
    mwSize dims[3];
    uint32_t nbytes;
    // Post inverse depth
    dims[0]         = trans.nv * trans.nu; 
    dims[1]         = 1; 
    dims[2]         = 1; 
    tmp             = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
    float * pt0     = mxGetPr(tmp);
    nbytes          = sizeof(float) * (trans.nv * trans.nu);
    memcpy (pt0, trans.rho, nbytes);

    plhs[0]         = tmp;
    
    // Post image
    dims[0]         = trans.nv; 
    dims[1]         = trans.nu; 
    dims[2]         = trans.nc; 
    tmp             = mxCreateNumericArray(3, dims, mxUINT8_CLASS, mxREAL);
    uint8_t * pt1   = mxGetPr(tmp);
    nbytes          = sizeof(uint8_t) * (trans.nv * trans.nu * 3);
    memcpy (pt1, trans.image, nbytes);

    plhs[1]         = tmp;
    
    DEBUG_GET_RENDER("Outputs posted");

    // Clean up
    DEBUG_GET_RENDER("Free");
    render_cleanup();
    lcm_destroy(lcm);

    
    DEBUG_GET_RENDER("Exit");
    
}

 