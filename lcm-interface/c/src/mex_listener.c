// file: send_message.c
//
// LCM example program.
//
// compile with:
//  $ gcc -o send_message send_message.c -llcm
//
// On a system with pkg-config, you can also use:
//  $ gcc -o send_message send_message.c `pkg-config --cflags --libs lcm`
#include "mex.h"
#include "matrix.h"


#include <lcm/lcm.h>
#include <stdio.h>

#include "../unity/unity_ctl_t.h"
#include "../unity/unity_render_raw_packet_t.h"


bool ctl_has_received 	= false;
bool raw_has_received   = false;

static void ctl_handler(const lcm_recv_buf_t *rbuf, const char *channel, const unity_ctl_t *msg,
                       void *user)
{
    int i;
    ctl_has_received 	= true;
    printf("Received message on channel \"%s\":\n", channel);
    // ctl_msg             = ctl_t();
    // ctl_msg.rCNu        = [1,0,0];
    // ctl_msg.Qur         = [1,0,0,0];;

    // ctl_msg.runRender   = 1    // ctl_msg.doGeoDepth  = 1;
    // ctl_msg.id          = 1;
    // ctl_msg.pkt_initID  = 1;
    printf("  rCNu          = (%f, %f, %f)\n", msg->rCNu[0], msg->rCNu[1], msg->rCNu[2]);
    printf("  Qur           = (%f, %f, %f, %f)\n", msg->Qur[0], msg->Qur[1],
           msg->Qur[2], msg->Qur[3]);
    printf("  runRender     = %d\n", msg->runRender);
    printf("  doGeoDepth    = %d\n", msg->doGeoDepth);
    printf("  id            = %d\n", msg->id);
    printf("  pkt_initID    = %d\n", msg->pkt_initID);

}
static void raw_handler(const lcm_recv_buf_t *rbuf, const char *channel, const unity_render_raw_packet_t *msg,
                       void *user)
{
    int i;
    raw_has_received    = true;
    printf("Received message on channel \"%s\":\n", channel);
   

}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // ctl_has_received    = false;
    // raw_has_received    = false;


    lcm_t *lcm      = lcm_create(NULL);
    if (!lcm)
        return;

    unity_ctl_t_subscribe(lcm, "UNITY_CTL", &ctl_handler, NULL);
    unity_render_raw_packet_t_subscribe(lcm, "UNITY_RENDER_RAW_PACKET", &raw_handler, NULL);
    
    while (!ctl_has_received || !raw_has_received){
        lcm_handle(lcm);
    }

    lcm_destroy(lcm);
}
 