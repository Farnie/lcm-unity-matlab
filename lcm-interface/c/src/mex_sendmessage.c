// file: send_message.c
//
// LCM example program.
//
// compile with:
//  $ gcc -o send_message send_message.c -llcm
//
// On a system with pkg-config, you can also use:
//  $ gcc -o send_message send_message.c `pkg-config --cflags --libs lcm`

#include <lcm/lcm.h>
#include <stdio.h>

#include "unity_ctl_t.h"
#include "mex.h"
#include "matrix.h"


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    lcm_t *lcm = lcm_create(NULL);
    if (!lcm)
    	return ;

    // unity_ctl_t my_data;
    unity_ctl_t my_data = {
        .rCNu = {1,0,0}, .Qur = {1, 0, 0, 0}, .runRender = 0, . doGeoDepth =0, .id=0, .pkt_initID=0
    };
    

    unity_ctl_t_publish(lcm, "EXAMPLE", &my_data);

    lcm_destroy(lcm);
}
