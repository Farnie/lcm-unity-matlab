#include <unistd.h>
#include "mex.h"
#include "matrix.h"
#include "sub_getLens.h"


#include <stdio.h>

#include "unity_ctl_t.h"
#include "unity_lens_t.h"

#define  LCM_TIMEOUT_MILLIS     1500



static unity_lens_t   *local_lens;

static int npkts_recv_lens       = 0;

static bool recv_lens     = false;


// Local prototypes


// ----------------------------------------- HANDLERS -----------------------------------------


static void lens_handler(const lcm_recv_buf_t *rbuf, const char *channel, const unity_lens_t *msg,
                       void *user)
{
    int i;
    
    DEBUG_GET_LENS("Received message on channel \"%s\":", channel);
    
    local_lens = unity_lens_t_copy(msg);

    recv_lens = true;
}
 



// ----------------------------------------- MAIN FUNCTION -----------------------------------------

int getLens(lcm_t *lcm, trans_t *t, unity_ctl_t * msg)
{   
    int retval = LCM_SUCCESS;
    DEBUG_GET_LENS("Entry");
    

    // print_unity_ctl_msg(msg);
    // LCM
    if (!lcm){
        DEBUG_GET_LENS("LCM Failed - Early Exit");
        return LCM_INIT_ERR;
    }
    //  Subscribe
    unity_lens_t_subscribe(lcm, "UNITY_LENS", &lens_handler, NULL);


    DEBUG_GET_LENS("Publishing to UNITY_CTL");
    unity_ctl_t_publish(lcm, "UNITY_CTL", msg);

    // Get result
    int ntrys = 5;
    for (int t = 0; t < ntrys; t++){
        int err = lcm_handle_timeout(lcm, LCM_TIMEOUT_MILLIS);
        if(err==0){
            DEBUG_GET_LENS("LCM HANDLE TIMED OUT");
            retval  = LCM_TIMEOUT;
        }
        if(err<0){
            DEBUG_GET_LENS("LCM HANDLE ERROR");
            retval  = LCM_FAIL;
            DEBUG_GET_LENS("Exit");
         return retval;
        }
        if(recv_lens){
            DEBUG_GET_LENS("LCM Received packages");
            retval  = LCM_SUCCESS;
            break;
        }
    }

    
    // All messages must be valid now
    // 
    // Put result into trans container
    ASSERT_GET_LENS(retval == LCM_SUCCESS, "LCM Messages not all received after %d trys", ntrys);
    ASSERT_GET_LENS(msg->id == local_lens->id, "Msg and recieved lens id are inconsitent. Expected: %d, Actual: %d", msg->id, local_lens->id);
    trans_moveToContainer(t);
    // if(retval == LCM_SUCCESS){
    // }else{
    //     // char buf[80];
    //     // sprintf(buf, "LCM Messages not all received after %d trys", ntrys);
    //     // mexErrMsgIdAndTxt("tim_mex:inputs", buf);
    //     ERROR();
    // }
    




    // Destroy things
    // cleanup();

    DEBUG_GET_LENS("Exit");
    return retval;
}



// ----------------------------------------- GENERIC -----------------------------------------

void trans_init(trans_t * t){
    t->nrays        = 0;
    t->proj_numel   = 0;
    t->proj         = 0;
    t->rQCr1        = 0;
    t->rQCr2        = 0;
    t->rQCr3        = 0;
}


void trans_moveToContainer(trans_t * t){
    DEBUG_GET_LENS("Enter");
   

    t->nrays        = local_lens->nrays;
    t->proj_numel   = local_lens->proj_numel;
    t->proj         = local_lens->proj;
    t->rQCr1        = local_lens->rQCr1;
    t->rQCr2        = local_lens->rQCr2;
    t->rQCr3        = local_lens->rQCr3;


    DEBUG_GET_LENS("Exit");
}


void lens_cleanup(){
    
    DEBUG_GET_LENS("Destroying lens msg");
    // unity_lens_t_destroy(local_lens);

    DEBUG_GET_LENS("Destroyed msgs");

}

// ----------------------------------------- PRINTERS -----------------------------------------

void print_unity_ctl_msg(unity_ctl_t * msg){
    DEBUG_GET_LENS("");
    printf("  rCNu              = (%g, %g, %g)\n", msg->rCNu[0], msg->rCNu[1], msg->rCNu[2]);
    printf("  Qur               = (%g, %g, %g, %g)\n", msg->Qur[0], msg->Qur[1],
           msg->Qur[2], msg->Qur[3]);
    printf("  runRender         = %d\n", msg->runRender);
    printf("  doGeoDepth        = %d\n", msg->doGeoDepth);
    printf("  id                = %d\n", msg->id);

}


void print_unity_lens_msg(unity_lens_t * msg){

    DEBUG_GET_LENS("");
    
    printf("  id                = %d\n", msg->id);
    printf("  nrays             = %d\n", msg->nrays);
}




