#ifndef GET_LENS_H
#define GET_LENS_H    

#include "math.h"
#include "stdio.h"
#include "stdlib.h"

#include "embed_debug.h"
#include "embed_timer.h"
#include <lcm/lcm.h>
#include "unity_ctl_t.h"
#include "unity_lens_t.h"

// #define __GET_LENS_DBG__
#define __GET_LENS_ERR__
#ifdef __GET_LENS_DBG__
    #define DEBUG_GET_LENS(...)  DEBUG_ALL(__VA_ARGS__)
#else
    #define DEBUG_GET_LENS(...) 
#endif

#ifdef __GET_LENS_ERR__
    #define ASSERT_GET_LENS(X, ...)  	ASSERT(X, __VA_ARGS__)
    #define ERROR_GET_LENS(...)  		ERROR(__VA_ARGS__)
#else
    #define ASSERT_GET_LENS(...) 
	#define ERROR_GET_LENS(...)
#endif




typedef struct 
{
	int32_t    proj_numel;
    float      *proj;
    int32_t    nrays;
    float      *rQCr1;
    float      *rQCr2;
    float      *rQCr3;
}trans_t;


int getLens(lcm_t *lcm, trans_t *t, unity_ctl_t * msg);
void lens_cleanup();
void trans_init(trans_t * t);
void trans_moveToContainer(trans_t * t);

//  Printers
void print_unity_lens_msg(unity_lens_t * msg);
void print_unity_ctl_msg(unity_ctl_t * msg);



#define LCM_SUCCESS 	0
#define LCM_TIMEOUT 	-1
#define LCM_FAIL 		-2
#define LCM_INIT_ERR 	-3




#endif