#include <unistd.h>
#include "mex.h"
#include "matrix.h"
#include "sub_getRender.h"


#include <stdio.h>

#include "unity_ctl_t.h"
#include "unity_img_t.h"
#include "unity_rho_t.h"

#define  LCM_TIMEOUT_MILLIS     1500



static unity_img_t   *local_img;
static unity_rho_t   *local_rho;

static int npkts_recv_img       = 0;
static int npkts_recv_rho       = 0;

static bool recv_img     = false;
static bool recv_rho     = false;


// Local prototypes
void safefree(void *ptr);

// ----------------------------------------- HANDLERS -----------------------------------------


static void img_handler(const lcm_recv_buf_t *rbuf, const char *channel, const unity_img_t *msg,
                       void *user)
{
    int i;
    
    DEBUG_GET_RENDER("Received message on channel \"%s\":", channel);
    
    if(~recv_img){
        DEBUG_GET_RENDER("Saving msg. ID = %d", msg->id);
        local_img   = unity_img_t_copy(msg);    
        recv_img    = true;
    }else{
        DEBUG_GET_RENDER("Discarding msg");
    }


}
 
static void rho_handler(const lcm_recv_buf_t *rbuf, const char *channel, const unity_rho_t *msg,
                       void *user)
{
    int i;
    
    DEBUG_GET_RENDER("Received message on channel \"%s\":", channel);

    if(~recv_rho){
        DEBUG_GET_RENDER("Saving msg. ID = %d", msg->id);
        local_rho   = unity_rho_t_copy(msg);
        recv_rho    = true;
    }else{
        DEBUG_GET_RENDER("Discarding msg");
    }

}



// ----------------------------------------- MAIN FUNCTION -----------------------------------------

int getRender(lcm_t *lcm, trans_t *t, unity_ctl_t * msg)
{   
    int retval = LCM_SUCCESS;
    DEBUG_GET_RENDER("Entry");
    

    // print_unity_ctl_msg(msg);
    // LCM
    if (!lcm){
        DEBUG_GET_RENDER("LCM Failed - Early Exit");
        return LCM_INIT_ERR;
    }
    //  Subscribe
    unity_img_t_subscribe(lcm, "UNITY_RENDER_IMG", &img_handler, NULL);
    unity_rho_t_subscribe(lcm, "UNITY_RENDER_RHO", &rho_handler, NULL);

    // Clear any previous messages
    // DEBUG_GET_RENDER("Clearing previous messages");
    // recv_img     = true;
    // recv_rho     = true;
    // int err = lcm_handle_timeout(lcm, LCM_TIMEOUT_MILLIS);
    recv_img     = false;
    recv_rho     = false;
    #ifdef  __GET_RENDER_DBG__
        print_unity_ctl_msg(msg);
    #endif
    DEBUG_GET_RENDER("Publishing to UNITY_CTL. ID = %d", msg->id);
    unity_ctl_t_publish(lcm, "UNITY_CTL", msg);

    // Get result
    int ntrys = 5;
    for (int t = 0; t < ntrys; t++){
        int err = lcm_handle_timeout(lcm, LCM_TIMEOUT_MILLIS);
        if(err==0){
            DEBUG_GET_RENDER("LCM HANDLE TIMED OUT");
            retval  = LCM_TIMEOUT;
        }
        if(err<0){
            DEBUG_GET_RENDER("LCM HANDLE ERROR");
            retval  = LCM_FAIL;
            DEBUG_GET_RENDER("Exit");
         return retval;
        }
        if(recv_img && recv_rho){
            DEBUG_GET_RENDER("LCM Received packages");
            retval  = LCM_SUCCESS;
            break;
        }
    }

    
    // All messages must be valid now
    // 
    // Put result into trans container
    ASSERT_GET_RENDER(retval == LCM_SUCCESS, "LCM Messages not all received after %d trys", ntrys);
    ASSERT_GET_RENDER(msg->id == local_img->id, "Msg and recieved image id are inconsitent. Expected: %d, Actual: %d", msg->id, local_img->id);
    ASSERT_GET_RENDER(msg->id == local_rho->id, "Msg and recieved rho id are inconsitent. Expected: %d, Actual: %d", msg->id, local_rho->id);

    trans_moveToContainer(t);
    // Destroy things
    // cleanup();

    DEBUG_GET_RENDER("Exit");
    return retval;
}



// ----------------------------------------- GENERIC -----------------------------------------

void trans_init(trans_t * t){
    t->nv       = 0;
    t->nu       = 0;
    t->nc       = 0;
    t->image    = 0;
    t->rho      = 0;
    
}


void trans_moveToContainer(trans_t * t){
    DEBUG_GET_RENDER("Enter");
    uint32_t    nc,
                nv,
                nu,
                nrays,
                np;

    nv          = local_img->nv;
    nu          = local_img->nu;
    nc          = local_img->nc;
    nrays       = nv*nu;

    t->nv       = nv;
    t->nu       = nu;
    t->nc       = nc;

    np                          = nu*nv;

    uint32_t    j_im            = 0, 
                idxNext_im      = 0, 
                idxPrevious_im  = 0,
                idx_im, 
                idx_pkt_im,
                j_rho           = 0, 
                idxNext_rho     = 0, 
                idxPrevious_rho = 0,
                idx_rho, 
                idx_pkt_rho;
    
    bool moveNext_im, moveNext_rho;


    // Convert from byte array to float array
    t->rho  = (float * )local_rho->data;

    t->image= local_img->data;

    DEBUG_GET_RENDER("Exit");
}


void render_cleanup(){
    
    DEBUG_GET_RENDER("Destroying img msg");
    // unity_img_t_destroy(local_img);
    // safefree(local_img->data);

    DEBUG_GET_RENDER("Destroying rho msg");
    // unity_rho_t_destroy(local_rho);

    DEBUG_GET_RENDER("Destroyed msgs");

}

void safefree(void *ptr){
    
    // if(ptr){
    //     DEBUG_GET_RENDER("freeing ptr");
    //     free(ptr);
    // }

}

// ----------------------------------------- PRINTERS -----------------------------------------

void print_unity_ctl_msg(unity_ctl_t * msg){
    DEBUG_GET_RENDER("");
    printf("  rCNu              = (%g, %g, %g)\n", msg->rCNu[0], msg->rCNu[1], msg->rCNu[2]);
    printf("  Qur               = (%g, %g, %g, %g)\n", msg->Qur[0], msg->Qur[1],
           msg->Qur[2], msg->Qur[3]);
    printf("  runRender         = %d\n", msg->runRender);
    printf("  doGeoDepth        = %d\n", msg->doGeoDepth);
    printf("  id                = %d\n", msg->id);

}


void print_unity_img_msg(unity_img_t * msg){

    DEBUG_GET_RENDER("");
    
    printf("  nu                = %d\n", msg->nu);
    printf("  nv                = %d\n", msg->nv);
    printf("  nc                = %d\n", msg->nc);
    printf("  id                = %d\n", msg->id);
}




