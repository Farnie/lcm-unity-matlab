#ifndef GET_RENDER_H
#define GET_RENDER_H    

#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "embed_mem.h"
#include "embed_debug.h"
#include <lcm/lcm.h>
#include "unity_ctl_t.h"
#include "unity_rho_t.h"
#include "unity_img_t.h"

// #define __GET_RENDER_DBG__
#define __GET_RENDER_ERR__
#ifdef __GET_RENDER_DBG__
    #define DEBUG_GET_RENDER(...)  DEBUG_ALL(__VA_ARGS__)
#else
    #define DEBUG_GET_RENDER(...) 
#endif

#ifdef __GET_RENDER_ERR__
    #define ASSERT_GET_RENDER(X, ...)  	ASSERT(X, __VA_ARGS__)
    #define ERROR_GET_RENDER(...)  		ERROR(__VA_ARGS__)
#else
    #define ASSERT_GET_RENDER(...) 
	#define ERROR_GET_RENDER(...)
#endif




typedef struct 
{
	int nv, nu, nc;
	uint8_t *image;
	float 	*rho;
}trans_t;


int getRender(lcm_t *lcm, trans_t *t, unity_ctl_t * msg);
void render_cleanup();
void trans_init(trans_t * t);
void trans_moveToContainer(trans_t * t);

//  Printers
void print_unity_ctl_msg(unity_ctl_t * msg);
void print_unity_img_msg(unity_img_t * msg);



#define LCM_SUCCESS 	0
#define LCM_TIMEOUT 	-1
#define LCM_FAIL 		-2
#define LCM_INIT_ERR 	-3




#endif