function key = checksum(rCNu,Qur)
%CHECKSUM Summary of this function goes here
%   Detailed explanation goes here
    pose    = [rCNu;Qur];
    a       = [typecast(pose,'uint8')];
    na      = length(a);
    key     = uint8(255);
    for i= 1:na
        key     = bitxor(key, a(i));
    end
end

