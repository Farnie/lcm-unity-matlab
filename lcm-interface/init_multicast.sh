# #!/bin/sh


BUFFER_SIZE=5621440

OS_LINUX="Linux"
OS_MACOS="Darwin"

OS="$(uname)"
echo $OS
if [ "${OS}" = "${OS_LINUX}" ]; then
    echo "OS is linux."
    sudo sysctl -w net.core.rmem_max=${BUFFER_SIZE}
    sudo sysctl -w net.core.rmem_default=${BUFFER_SIZE}
elif [ "${OS}" = "${OS_MACOS}" ]; then
    echo "OS is Mac."
    sudo sysctl -w net.inet.udp.recvspace=${BUFFER_SIZE} && 
	sudo sysctl -w net.inet.udp.maxdgram=${BUFFER_SIZE}
else
	echo "Operating system, ${OS}, not accounted for"
	exit
fi


# VAR1="Linuxize"
# VAR2="Linuxize"

# if [ "$VAR1" = "$VAR2" ]; then
#     echo "Strings are equal."
# else
#     echo "Strings are not equal."
# fi
# sudo sysctl -w net.inet.udp.recvspace=${BUFFER_SIZE} && 
# sudo sysctl -w net.inet.udp.maxdgram=${BUFFER_SIZE}