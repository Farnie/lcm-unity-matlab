function [err, emsg] = checkPacketAlignment(expected_id, packetdata, name)
	packet_id			= packetdata.packet_id;
	emsg				= "";
	err					= false;
	
	if(expected_id~=packet_id)
		emsg				= sprintf('Missing %s packet\n >> id %-4d | expected pkt-id %d | received pkt-id %d |\n', name, packetdata.id, expected_id, packet_id);
		fprintf(2,emsg);
		err					= true;
	end
end