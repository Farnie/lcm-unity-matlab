function msg = getLCMMessage(aggregator, chname)

	millis_to_wait = 1000;
	while true
		msg = aggregator.getNextMessage(millis_to_wait);
		if length(msg) > 0
% 			fprintf('%s received\n', msg.channel);
			
			return;
		else
			errmsg		= sprintf('Timeout on channel %s\n', chname);
			fprintf(2, errmsg);
			error(errmsg);
		end
	end
end
