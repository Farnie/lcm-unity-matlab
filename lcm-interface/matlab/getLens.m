function trans = getLens(lc, agg_header, agg_packet, msg)
	pkt_failID	= 0;
	transall	= struct;
	transi		= [];
	l			= [];
	i			= 1;
    notdone     = true;
	while notdone
		try
			getAllFromAggregator(agg_header);
			getAllFromAggregator(agg_packet);
			
			msg.pkt_initID		= pkt_failID;
			[transi, pkt_failID, l] = lensTransaction(lc, ...
								  agg_header, ...
								  agg_packet, ...
								  msg);
			transall(i).data			= transi;	
			transall(i).pkt_initID		= msg.pkt_initID;
			transall(i).pkt_failID		= pkt_failID;
            notdone     = transi.msgs(end) ~= l.nmsgs;
			i			= i +1;
            
		catch me
% 			rethrow(me);
			fprintf(2, 'Lens transaction failed at packet ID %d!\n', pkt_failID);
            fprintf(2, 'Cause: \n%s\n', me.message);
			fprintf(2, 'Trying again\n');
            
			transi = [];
			
			getAllFromAggregator(agg_header);
			getAllFromAggregator(agg_packet);
		end
	end

	%% Extract Packets
	nt		= length(transall);
	nrays	= l.nrays;
	rQCr	= zeros(3, nrays);
    tnrays  = zeros(nt+1);
    for i = 1:nt
        tnrays(i+1)	= transall(i).data.nrays;
    end
	
	for i = 1:nt
        transi      = transall(i).data;
		nraysi		= transi.nrays;
		idx			= (1:nraysi) + tnrays(i);
		rQCr(:,idx) = transi.rQCr;
	end
	
	proj		=  transall(1).data.proj;
	
	nrQCr		= sqrt(sum(rQCr.^2,1));
	erridx		= abs(nrQCr - 1)>1e-6;
	assert(sum(erridx)==0, 'Data is corrupted');
	
	trans.rQCr	= rQCr;
	trans.nrays = sum(tnrays);
	trans.proj	= proj;
	
end

function [trans, pkt_failID, l] = lensTransaction(lc, agg_header, agg_packet, msg)
	
	pkt_failID			= msg.pkt_initID;
	lc.publish('UNITY_CTL', msg);
    
    
	trans				= [];
	% Await response
	lens_head			= getLCMMessage(agg_header, 'lens header');
		
	l					= unity.lens_header_t(lens_head.data);
	nrays				= l.nrays;
	nmsgs				= l.nmsgs;
	max_packet_length	= l.max_packet_length;
	rQCr				= zeros(3, nrays);
	proj				= zeros(4);
	proj(:)				= l.proj;
	
	idxInit				= (pkt_failID)*max_packet_length + 1;
	idx					= [];
	
	for i = (pkt_failID + 1):nmsgs
		packet				= getLCMMessage(agg_packet,'lens packet');
		packetdata			= unity.lens_packet_t(packet.data);

		if(checkPacketAlignment(i, packetdata, 'lens'))
			pkt_failID	= i-1;
			break;
		end
		
		sub_rQCr1			= packetdata.rQCr1';
		sub_rQCr2			= packetdata.rQCr2';
		sub_rQCr3			= packetdata.rQCr3';
		
		len					= packetdata.packet_length;
		idx					= (i-1)*max_packet_length + (1:len);
		rQCr(:,idx)			= [sub_rQCr1; sub_rQCr2; sub_rQCr3];
		
	end

	if(isempty(idx))
		trans.rQCr	= zeros(3,0);
	else
		idxEnd		= idx(end);
		idxOI		= idxInit:idxEnd;
		trans.rQCr	= rQCr(:,idxOI);

	end
	trans.msgs 	= (pkt_failID + 1):i;
	trans.nrays = size(trans.rQCr,2);
	trans.proj	= proj;
	
end




