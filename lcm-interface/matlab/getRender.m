function trans = getRender(lc, aggreg_rend_head, aggreg_rend_rho_pkt, aggreg_rend_raw_pkt, msg, id)
    % Post Message
	pkt_failID	= 0;
	transall	= struct;
	transi		= [];
	l			= [];
	i			= 1;
	notdone     = true;
	while notdone
		try
			getAllFromAggregator(aggreg_rend_head);
			getAllFromAggregator(aggreg_rend_rho_pkt);
			getAllFromAggregator(aggreg_rend_raw_pkt);
			msg.pkt_initID		= pkt_failID;

			[transi, pkt_failID, r] = renderTransaction(lc, ...
								  aggreg_rend_head, ...
								  aggreg_rend_rho_pkt, ...
								  aggreg_rend_raw_pkt, ...
								  msg);

			transall(i).data			= transi;	
			transall(i).pkt_initID		= msg.pkt_initID;
			transall(i).pkt_failID		= pkt_failID;

			nmsgs 		= max(r.nmsgs_raw, r.nmsgs_rho);
			notdone     = transi.msgs(end) ~= nmsgs;
			i			= i +1;
		catch me
			rethrow(me);
			fprintf(2, 'Transaction %d failed!\n', id);
			fprintf(2, 'Cause: \n%s\n', me.message);
			fprintf(2, 'Trying again\n');

			transi = [];

			getAllFromAggregator(aggreg_rend_head);
			getAllFromAggregator(aggreg_rend_rho_pkt);
			getAllFromAggregator(aggreg_rend_raw_pkt);
		end
	end

	%% Extract Packets
	nrays 			= r.nrays;
	nv 				= r.nv;
	nu 				= r.nu;
	nRawBytes 		= r.nRawBytes;
    nChBytes        = r.nChBytes;
	nt				= length(transall);
	rho				= zeros(1, nrays);
	raw_r_bytes		= int8(zeros(1, nChBytes));
	raw_g_bytes		= int8(zeros(1, nChBytes));
	raw_b_bytes		= int8(zeros(1, nChBytes));
	for i = 1:nt
		transi 	= transall(i).data;
		rho_idx = transi.rho_idx;
		sub_rho = transi.rho;
		if(~isempty(rho_idx))
			rho(rho_idx) = sub_rho;
		end
		raw_idx 	= transi.raw_idx;
		sub_raw_r 	= transi.raw_r_bytes;
		sub_raw_g 	= transi.raw_g_bytes;
		sub_raw_b 	= transi.raw_b_bytes;
		if(~isempty(raw_idx))
			raw_r_bytes(raw_idx) = sub_raw_r;
			raw_g_bytes(raw_idx) = sub_raw_g;
			raw_b_bytes(raw_idx) = sub_raw_b;
		end
	end
	raw_bytes 		= [raw_r_bytes; raw_g_bytes; raw_b_bytes];
	raw_bytes_col 	= raw_bytes(:);
	trans.image 	= rawBytesToImg(raw_bytes_col,nv,nu);
	trans.rho 		= rho;
	

end

function [trans, pkt_failID, r] = renderTransaction(lc, agg_header, agg_rho_packet, agg_raw_packet, msg)
	
	pkt_failID			= msg.pkt_initID;
	lc.publish('UNITY_CTL', msg);

	trans				= [];
	% Await response
	pause(0.1);
	render_head			= getLCMMessage(agg_header, 'render header');
		
	r					= unity.render_header_t(render_head.data);
	nrays				= r.nrays;
	nRawBytes			= r.nRawBytes;
	nChBytes			= r.nChBytes;
	nu					= r.nu;
	nv					= r.nv;
	nmsgs_rho			= r.nmsgs_rho;
	nmsgs_raw			= r.nmsgs_raw;
	nmsgs				= max(nmsgs_raw, nmsgs_rho);
	
	max_packet_length	= r.max_packet_length;
	rho					= zeros(1, nrays);
	
	raw_r_bytes			= int8(zeros(1, nChBytes));
	raw_g_bytes			= int8(zeros(1, nChBytes));
	raw_b_bytes			= int8(zeros(1, nChBytes));

	idxInit				= (pkt_failID)*max_packet_length + 1;

    rho_idx             = [];
    raw_idx             = [];
    
	for i = (pkt_failID + 1):nmsgs
		if(i<=nmsgs_rho)
			packet				= getLCMMessage(agg_rho_packet, 'rho');
			packetdata			= unity.render_rho_packet_t(packet.data);

			if(checkPacketAlignment(i, packetdata, 'rho'))
				pkt_failID			= i-1;
				break;
			end

			sub_rho				= packetdata.rho;
			len					= packetdata.packet_length;
			rho_idx				= (i-1)*max_packet_length + (1:len);
			rho(rho_idx)		= sub_rho;
		end
		
		if(i<=nmsgs_raw)
			packet				= getLCMMessage(agg_raw_packet, 'raw');
			packetdata			= unity.render_raw_packet_t(packet.data);
			[err,emsg]			= checkPacketAlignment(i, packetdata, 'raw');
			if(checkPacketAlignment(i, packetdata, 'raw'))
				pkt_failID			= i-1;
				break;
			end
			
			sub_raw_r				= packetdata.raw_r;
			sub_raw_g				= packetdata.raw_g;
			sub_raw_b				= packetdata.raw_b;

			len						= packetdata.packet_length;
			raw_idx					= (i-1)*max_packet_length + (1:len);
			raw_r_bytes(raw_idx)	= sub_raw_r;
			raw_g_bytes(raw_idx)	= sub_raw_g;
			raw_b_bytes(raw_idx)	= sub_raw_b;
		end
	end
	
	if(~isempty(rho_idx))
		rho_idxEnd		= rho_idx(end);
		trans.rho_idx 	= idxInit:rho_idxEnd;
		trans.rho 		= rho(trans.rho_idx);
	else
		trans.rho_idx 	= [];
		trans.rho 		= [];
	end

	if(~isempty(raw_idx))
		raw_idxEnd			= raw_idx(end);
		trans.raw_idx 		= idxInit:raw_idxEnd;
		trans.raw_r_bytes 	= raw_r_bytes(trans.raw_idx);
		trans.raw_g_bytes 	= raw_g_bytes(trans.raw_idx);
		trans.raw_b_bytes 	= raw_b_bytes(trans.raw_idx);
	else
		trans.raw_idx 	= [];
		trans.raw_bytes = [];
	end
	
	trans.msgs 		= (pkt_failID + 1):i;
	trans.nrays 	= nrays;
	trans.nv		= nv;
	trans.nu		= nu;
end