function [im, err] = rawBytesToImg(raw_bytes,nv,nu)
		
	% convert from int8 to unit8
	raw_bytesu	= uint8(double(raw_bytes) + 255*(raw_bytes<0));
	im 			= uint8(zeros(nv, nu, 3));
	np 			= nv*nu;
	pselect		= 3*((np:-1:1)-1);
	ridx		= pselect + 1;
	bidx		= pselect + 2;
	gidx		= pselect + 3;
	r 			= raw_bytesu(ridx);
	g 			= raw_bytesu(bidx);
	b 			= raw_bytesu(gidx);
	im(:,:,1)	= reshape(r, nu, nv)';
	im(:,:,2)	= reshape(g, nu, nv)';
	im(:,:,3) 	= reshape(b, nu, nv)';
	im			= im(:,end:-1:1,:);
	err 		= 0;
end