function [rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur, doGeo)
%SETPOSEGETFRAME Summary of this function goes here
%   Detailed explanation goes here
	persistent lc;
	persistent aggreg_rend_head aggreg_rend_rho_pkt aggreg_rend_raw_pkt aggreg_lens_head aggreg_lens_pkt;
	persistent id rQCrp projp Kp;
		
	if(isempty(lc))
		addjars;
		id						= 0;
		
		ch						= 'udpm://239.255.76.67:7667?ttl=1';
		lch						= lcm.lcm.LCM(ch);
		lc						= lch;

		aggreg_rend_head		= lcm.lcm.MessageAggregator();
		aggreg_rend_rho_pkt		= lcm.lcm.MessageAggregator();
		aggreg_rend_raw_pkt		= lcm.lcm.MessageAggregator();
		aggreg_lens_head		= lcm.lcm.MessageAggregator();
		aggreg_lens_pkt			= lcm.lcm.MessageAggregator();
		
		lc.subscribe('UNITY_RENDER_HEADER',		aggreg_rend_head);
		lc.subscribe('UNITY_RENDER_RHO_PACKET', aggreg_rend_rho_pkt);
		lc.subscribe('UNITY_RENDER_RAW_PACKET', aggreg_rend_raw_pkt);
		lc.subscribe('UNITY_LENS_HEADER',		aggreg_lens_head);
		lc.subscribe('UNITY_LENS_PACKET',		aggreg_lens_pkt);
		rQCrp	= [];
		msg				= unity.ctl_t();
		msg.rCNu		= rCNu;
		msg.Qur			= Qur;
		msg.runRender	= 0;
		msg.pkt_initID 	= 0;
		ltrans			= getLens(lc, aggreg_lens_head, aggreg_lens_pkt, msg);
		rQCrp			= ltrans.rQCr;
		projp			= ltrans.proj;
	end
	id = id + 1;
	
	if(nargin~=3)
		doGeo			= true;
	end
	
	% Prepare Message
	msg				= unity.ctl_t();
	msg.rCNu		= rCNu;
	msg.Qur			= Qur;
	msg.runRender	= 1;
	msg.id			= id;
	msg.doGeoDepth	= doGeo;
	
	% Post Message
	trans       = getRender(lc, aggreg_rend_head, aggreg_rend_rho_pkt, aggreg_rend_raw_pkt, msg, id);
	
	fprintf('Transaction %d complete \n', id);
	rho			= trans.rho;
	image		= trans.image;
	rQCr		= rQCrp;
	proj		= projp;
end
