import sys 
import os


import lcm

from unity import *



def my_handler(channel, data):
    msg = render_raw_packet_t.decode(data)
    print("Received message on channel \"%s\"" % channel)
    # print(msg.raw_r)
    # print("   raw_r         = %s" % str(msg.raw_r));
    # print("   raw_g         = %s" % str(msg.raw_g));
    # print("   raw_b         = %s" % str(msg.raw_b));
    print("   id            = %s" % str(msg.id));
    print("   packet_id     = %s" % str(msg.packet_id));
    

lc = lcm.LCM()
subscription = lc.subscribe("EXAMPLE", my_handler)

try:
    while True:
        lc.handle()
except KeyboardInterrupt:
    pass

lc.unsubscribe(subscription)
