function reloadPy()
    warning('off','MATLAB:ClassInstanceExists')
    clear classes
    delete *.pyc
    mod = py.importlib.import_module('unityrender');
    py.reload(mod);
    warning('on','MATLAB:ClassInstanceExists')