clc
clear


% reloadPy();

% clear classes
% mod = py.importlib.import_module('unityrender');
% py.reload(mod);
% 
% try
%     !rm *.pyc
% catch
%     
% end
% a = @()(py.unityrender);
% 
% res1 = py.unityrender.runRender();
% res2 = py.unityrender.setPoseGetFrame(0,0,0);

rCNu    = zeros(3,1);
Qur     = [zeros(3,1); 1];
doGeo   = true;
[rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur, doGeo);

function [rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur, doGeo)
    persistent isinit 
    if(isempty(isinit))
        reloadPy();
        isinit = true;
    end
    res     = py.unityrender.renderUnity(rCNu,Qur,doGeo);
    rho     = res(1).cell{:};
    image   = res(2).cell{:};
    rQCr  	= res(3).cell{:};
    proj  	= res(4).cell{:};
end
