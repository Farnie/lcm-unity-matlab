function [rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur, doGeo)
    persistent isinit 
    if(isempty(isinit))
        reloadPy();
        isinit = true;
    end
    res     = py.unityrender.renderUnity(rCNu,Qur,doGeo);
    rho     = res(1).cell{:};
    image   = res(2).cell{:};
    rQCr  	= res(3).cell{:};
    proj  	= res(4).cell{:};
end
