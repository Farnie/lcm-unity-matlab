import lcm
import timeit
import random
import string

from unity import *


lc 					= lcm.LCM()


ctl_msg 			= ctl_t();
ctl_msg.rCNu		= [1,0,0];
ctl_msg.Qur 		= [1,0,0,0];
ctl_msg.runRender 	= 1;
ctl_msg.doGeoDepth 	= 1;
ctl_msg.id 			= 1;
ctl_msg.pkt_initID 	= 1;

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


rend_msg 			= render_raw_packet_t()
nv 					= 1200;
nu 					= 720;
np 					= nu*nv;
print("Generate Strings")
n 					= np;
rend_msg.packet_length 	= n;
rend_msg.raw_r 			= randomString(n);
rend_msg.raw_g 			= randomString(n);
rend_msg.raw_b 			= randomString(n);

# def postImage():
# 	lc.publish("UNITY_RENDER_RAW_PACKET", rend_msg.encode())

def postHeader():
	lc.publish("UNITY_CTL", ctl_msg.encode())

print("Publish Strings")



# print(" %s " %str(rend_msg.raw_r))
# t0 	= time.process_time_ns(); 


postHeader();
# postImage();

# niter 	= 1;
elapsed_time = 0;
# elapsed_time = timeit.timeit(postImage, number=niter)/niter
# tf 	= time.process_time_ns();

# dt	 = tf - t0;

print("Done Strings. Posted in %g" %(elapsed_time))


