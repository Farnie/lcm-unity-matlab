"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class ctl_t(object):
    __slots__ = ["id", "rCNu", "Thetaur", "runRender", "doGeoDepth"]

    __typenames__ = ["int32_t", "float", "float", "int8_t", "int8_t"]

    __dimensions__ = [None, [3], [3], None, None]

    def __init__(self):
        self.id = 0
        self.rCNu = [ 0.0 for dim0 in range(3) ]
        self.Thetaur = [ 0.0 for dim0 in range(3) ]
        self.runRender = 0
        self.doGeoDepth = 0

    def encode(self):
        buf = BytesIO()
        buf.write(ctl_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">i", self.id))
        buf.write(struct.pack('>3f', *self.rCNu[:3]))
        buf.write(struct.pack('>3f', *self.Thetaur[:3]))
        buf.write(struct.pack(">bb", self.runRender, self.doGeoDepth))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != ctl_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return ctl_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = ctl_t()
        self.id = struct.unpack(">i", buf.read(4))[0]
        self.rCNu = struct.unpack('>3f', buf.read(12))
        self.Thetaur = struct.unpack('>3f', buf.read(12))
        self.runRender, self.doGeoDepth = struct.unpack(">bb", buf.read(2))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if ctl_t in parents: return 0
        tmphash = (0xa217ffb29da71cb4) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff) + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if ctl_t._packed_fingerprint is None:
            ctl_t._packed_fingerprint = struct.pack(">Q", ctl_t._get_hash_recursive([]))
        return ctl_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

