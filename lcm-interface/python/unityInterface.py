import timeit
import random
import string
import sys
import os
import subprocess

from unity import *
import struct
import select
from threading import Thread
import time
import math

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import lcm
import torch
import numpy as np
from pathlib import Path

DIR_THIS            = os.path.dirname(os.path.realpath(__file__))
DIR_GIT             = os.path.abspath(os.path.join(DIR_THIS, "..", "..", ".."))
DIR_UNITY           = os.path.join(DIR_THIS ,  'unity')
DIR_PYTORCH_VISION  = os.path.join(DIR_GIT ,  'PycharmProjects', "pytorch-vision")
DIR_PYTORCH_LIB     = os.path.join(DIR_PYTORCH_VISION , "libraries")

sys.path.append(DIR_UNITY)
sys.path.append(DIR_PYTORCH_LIB)

import general as gn

home_dir        = str(Path.home())
UNITY_CTL       = os.path.join(home_dir, "UNITY_CTL")
UNITY_LENS      = os.path.join(home_dir, "UNITY_LENS")
UNITY_IMG       = os.path.join(home_dir, "UNITY_RENDER_IMG")
UNITY_RHO       = os.path.join(home_dir, "UNITY_RENDER_RHO")


class subscription:
    def __init__(self, channel, msg_type, disp=False):
        self.channel        = channel
        self.msg_type       = msg_type
        self.counter        = 0
        self.received       = False
        self.subscription   = None
        self.msg_decoded    = None
        self.disp           = disp

    def handler(self, channel, data):
        if(self.disp):
            print("Received message on channel \"%s\"" % channel)
        msg             = self.msg_type.decode(data)
        self.msg_decoded= msg
        self.received   = True

    def getMsg(self):
        return self.msg_decoded

    def _print(self):
        print("          channel: %s" % str(self.channel))
        print("         msg_type: %s" % str(self.msg_type))
        print("          counter: %s" % str(self.counter))
        print("         received: %s" % str(self.received))




Ric = torch.tensor(
    [
        [0, 1., 0],
        [0, 0, 1.],
        [1., 0, 0]
    ])
 
class UnityInterface(object):
    """docstring for UnityInterface"""
    def __init__(self, device='cpu', dtype=torch.float, disp=False):
        # elevate()
        # self.initMultiCast()
        self.device                 = device
        self.dtype                  = dtype

        self.lcm                    = lcm.LCM()
        # LCM data
        self.recv_lens              = None
        self.recv_img               = None
        self.recv_rho               = None

        self.rQCr                   = None
        self.proj                   = torch.empty([4,4])

        self.msg_numtrys            = 10    # Number of trys
        self.msg_timeout_ms         = 1000  # Time to wait in milliseconds
        self.msg_timeout_ms_clear   = 5
        self.disp                   = disp


        self.Tnu         = torch.tensor([[0,0,1], [1,0,0], [0,-1,0]], device=self.device, dtype=self.dtype)
        self.Tun         = self.Tnu.t()

        assert torch.abs(self.Tnu.det() + 1).item() < 1e-6, "Expected Tnu to be O(3)"


        # Map from unity body coordinates to NED body fixed coordinates
        self.Tcr         = torch.tensor([[1,0,0], [0,-1,0], [0,0,1]], device=self.device, dtype=self.dtype)
        assert torch.abs(self.Tcr.det() + 1).item() < 1e-6, "Expected Tnu to be O(3)"

        self.getLens()

        self.rQCc       = torch.matmul(self.Tcr, self.rQCr)

        rCNn        = torch.tensor([[-217.0],[191.0],[-345.0]])
        Rnc         = torch.eye(3)
        self.renderUnity(rCNn, Rnc, 0)

    

    def renderUnity(self, rCNn, Rnc, doGeo=False):
        # Request render given NED coordinates

        type_Rnc    = type(Rnc).__module__
        type_rCNn   = type(rCNn).__module__
        type_numpy  = 'numpy'
        if(type_rCNn == type_numpy):
            rCNn    = torch.from_numpy(rCNn)

        if (type_Rnc == type_numpy):
            Rnc     = torch.from_numpy(Rnc)


        rCNn        = rCNn.to(dtype=self.dtype, device=self.device)
        Rnc         = Rnc.to(dtype=self.dtype, device=self.device)

        Rnc         = Rnc.reshape([3,3])
        rCNu        = torch.matmul(self.Tun, rCNn)
        Rur         = torch.matmul(self.Tun, torch.matmul(Rnc, self.Tcr))
        Thetaur     = gn.rot2vec(Rur)
        return self.setPoseGetFrame(rCNu, Thetaur, doGeo=doGeo)




    def setPoseGetFrame(self, rCNu, Thetaur, doGeo=False):
        # Request render given unity coordinates
        ctl             = ctl_t()
        ctl.rCNu        = rCNu
        ctl.Thetaur     = Thetaur
        ctl.runRender   = 1
        ctl.doGeoDepth  = doGeo
        ctl.id          = self.checkSum(rCNu, Thetaur)

        return self.getRender(ctl)




    def getLens(self):
        ctl             = ctl_t()
        ctl.rCNu        = [200,400,-217]
        ctl.Thetaur         = [0,0,0,1]
        ctl.runRender   = 0 # Do not render, just request lens
        ctl.doGeoDepth  = 1
        ctl.id          = 0#self.checkSum(ctl.rCNu, ctl.Thetaur)

        
        
        
        # self.onceSubscription(UNITY_LENS, self.handler_lens)

        # channel_list, type_list = [], []
        # channel_list.append(UNITY_LENS)
        # type_list.append(lens_t)
        self.clearChannel(UNITY_LENS)

        subs = []
        subs.append(subscription(UNITY_LENS, lens_t))
        self.subscribe_init(subs)
        self.lcm.publish(UNITY_CTL, ctl.encode())

        out = self.subscribe_handle(subs)

        self.recv_lens  = out[UNITY_LENS]


        if(not(self.recv_lens == None)):
            msg             = self.recv_lens
            nu              = msg.nu
            nv              = msg.nv
            nd              = nu * nv

            

            # rQCr1_bytes     = msg.rQCr1_bytes
            # rQCr2_bytes     = msg.rQCr2_bytes
            # rQCr3_bytes     = msg.rQCr3_bytes

            # rQCr1           = np.frombuffer(rQCr1_bytes, dtype=np.single).reshape(nu, nv).T.flatten()
            # rQCr2           = np.frombuffer(rQCr2_bytes, dtype=np.single).reshape(nu, nv).T.flatten()
            # rQCr3           = np.frombuffer(rQCr3_bytes, dtype=np.single).reshape(nu, nv).T.flatten()

            # self.rQCr[0,:]  = torch.tensor(rQCr1, device=self.device,dtype=self.dtype)
            # self.rQCr[1,:]  = torch.tensor(rQCr2, device=self.device,dtype=self.dtype)
            # self.rQCr[2,:]  = torch.tensor(rQCr3, device=self.device,dtype=self.dtype)

            proj_flat       = torch.tensor(msg.proj, device=self.device,dtype=self.dtype)
            self.proj       = torch.reshape(proj_flat, (4,4)).t()

            L               = torch.eye(3, device=self.device,dtype=self.dtype)
            L[0:2, 0:2]     = self.proj[0:2, 0:2]

            u0              = (nu-1) / 2.0
            v0              = (nv-1) / 2.0
            Tmis = torch.tensor(
                [
                    [u0, 0, u0],
                    [0, v0, v0],
                    [0, 0, 1]
                ],
                dtype=self.dtype,
                device=self.device)

            K               = Tmis.matmul(L)

            self.Ki         = K

            xv, yv          = torch.meshgrid([torch.arange(0, nu), torch.arange(0, nv)])


            pb              = torch.ones([3,nd], device=self.device,dtype=self.dtype)
            pb[0, :]        = xv.t().reshape([1, nd])
            pb[1, :]        = yv.t().reshape([1, nd])
            rPCc,a          = pb.solve(K)
            rPCc_mag        = torch.norm(rPCc,dim=0, keepdim=True)

            self.p          = pb[0:2,:]
            rQCc            = rPCc/rPCc_mag
            self.rQCr,a     = torch.solve(rQCc, self.Tcr)
            
            self.nv         = nv
            self.nu         = nu


    
    def getRender(self, ctl):
        # Initalisation
        self.recv_img   = None
        self.recv_rho   = None
        
        # Post request
        self.clearChannel(UNITY_IMG)
        self.clearChannel(UNITY_RHO)

        subs = []
        subs.append(subscription(UNITY_IMG, img_t))
        subs.append(subscription(UNITY_RHO, rho_t))
        self.subscribe_init(subs)

        self.lcm.publish(UNITY_CTL, ctl.encode())
        out             = self.subscribe_handle(subs)

        self.recv_img   = out[UNITY_IMG]
        self.recv_rho   = out[UNITY_RHO]


        # ---------------- Image data -------------------

        img_bgr         = None
        self.nv         = self.recv_img.nv
        self.nu         = self.recv_img.nu
        self.nc         = self.recv_img.nc
        nd              = self.nv * self.nu

        dlong           = torch.long if self.device=='cpu' else torch.long
        idx_f           = torch.arange(nd, device=self.device,dtype=dlong)
        row             = torch.flip(idx_f,[0])
        pselect         = 3*(row)
        r_idx           = pselect
        g_idx           = pselect+1
        b_idx           = pselect+2
        data            = torch.tensor(list(self.recv_img.data),dtype=self.dtype)

        newshape        = ( self.nv, self.nu)
        r               = torch.reshape(data[r_idx],newshape)
        g               = torch.reshape(data[g_idx],newshape)
        b               = torch.reshape(data[b_idx],newshape)

        img_rgb         = np.empty([self.nv, self.nu, self.nc])
        img_rgb[:,:,0]  = r/255.0
        img_rgb[:,:,1]  = g/255.0
        img_rgb[:,:,2]  = b/255.0

        img_rgb         = img_rgb[:,::-1,:]

        # ---------------- Depth data -------------------
        # rho             = torch.empty([self.nv, self.nu])
        # rho_vec is in column major format
        data            = self.recv_rho.data
        rho_vec         = np.frombuffer(data, dtype=np.single)
        # https://kanoki.org/2020/01/06/reshaping-numpy-arrays-in-python/
        rho_np          = rho_vec.reshape(self.nv, self.nu)
        # Consistent with MATLAB
        rho_np          = 1.0*rho_np[::-1,:]

        rQCr3_mat       = self.rQCr[2,:].reshape( self.nv, self.nu)

        rho             = torch.from_numpy(rho_np)
        rho             = rho.to(device=self.device, dtype=self.dtype)

        # Correction for ray angle. Z depth to range
        if(ctl.doGeoDepth==0):
            rho         = rho * rQCr3_mat
            # rho_vec         = torch.reshape(rho, (1, -1))
            # rQCr3           = torch.reshape(self.rQCr[2,:], (1, -1))
            # # print(rho_vec)
            # # print(rQCr3)
            # rho_vec         = rho_vec*rQCr3
            # rho             = torch.reshape(rho_vec, ( self.nv, self.nu))
            # rho             = rho
        
        # rho             = rho.astype(float)
        

        return rho ,img_rgb

    # ----------------------- SUBSCRIPTION SERVICE -----------------------

    def clearChannel(self, channel):
        counter         = 0
        notdone         = True
        def null(channel, data):

            notdone         = False
            print("Discarding message from " + channel)
            return 0
        subscription    = self.lcm.subscribe(channel, null)

        # while notdone and counter<self.msg_numtrys:
        #     counter     += 1
        self.lcm.handle_timeout(self.msg_timeout_ms_clear)
        
        self.lcm.unsubscribe(subscription)            

    def subscribe_init(self, subs):
        for sub in subs:
            sub.subscription    = self.lcm.subscribe(sub.channel, sub.handler)

    def subscribe_handle(self, subs):
        # channel [ n x 1] list
        # handler [ n x 1] list

        allReceived     = False
        out             = dict()



        timeout     = self.msg_timeout_ms/1000
        try:
            while not allReceived:
                # self.lcm.handle_timeout(self.msg_timeout_ms)
                rfds, wfds, efds = select.select([self.lcm.fileno()], [], [], timeout)

                if rfds:
                    self.lcm.handle()
                else:
                    print("Waiting for message...")

                _allRec = True
                for sub in subs:
                    
                    if(sub.counter>self.msg_numtrys):
                        errorstring = "No message recieved on channel " + sub.channel + " after " + str(sub.counter) + " trys"
                        sys.exit(errorstring)

                    _allRec     &= sub.received
                    sub.counter += 1

                allReceived =  _allRec                   

        except KeyboardInterrupt:
            pass

        finally:
            for sub in subs:
                self.lcm.unsubscribe(sub.subscription)
                # sub.print()
                out[sub.channel] = sub.msg_decoded

        return out

    # ----------------------- MISC. HELPER FUNCTIONS -----------------------
    def initMultiCast(self):
        initScript   = os.path.join(DIR_THIS, '..' ,  './init_multicast.sh')
        # stream = os.popen(initScript)
        string      = "osascript -e 'do shell script \"" + initScript + "\" with administrator privileges'"
        print(string)
        os.system(string)

    def checkSum(self, rCNu, Thetaur):
        pose    = rCNu.flatten().tolist()
        pose.extend(Thetaur.flatten().tolist())

        packet = None
        checksum= 0xffffffff
        for i in range(0,len(pose)):
            # Extract byte tensor for each float in pose
            packet  = bytearray(struct.pack("d", float(pose[i])))  
            # Add each byte into xor checksum
            # for el in packet:
            el  = 0xffffffff & int.from_bytes(packet, byteorder='big', signed=False)
            checksum ^=el
        checksum = 0x7fffffff & checksum
        return checksum



# ---------------------- UNCOMMENT TO RUN EXAMPLE -----------------------
# print("Hi")        
# print("Init UnityInterface")

# a           = UnityInterface()   
# rCNn        = torch.tensor([[-217.0],[191.0],[-345.0]])
# Rnc         = torch.eye(3)
# rho,img     = a.renderUnity(rCNn, Rnc, 0)
 
# depth   = (1/rho).numpy()
# fig, (ax1, ax2) = plt.subplots(1, 2)
 
# ax1.imshow(depth, aspect=1, interpolation='none', origin='lower', vmin=0, vmax=50)
# ax1.set_xlabel("u - [pix]")
# ax1.set_ylabel("v - [pix]")
 
# ax2.imshow(img, aspect=1)
# ax2.set_xlabel("u - [pix]")
# ax2.set_ylabel("v - [pix]")
 
# fig.show()
 
# fig.clear


