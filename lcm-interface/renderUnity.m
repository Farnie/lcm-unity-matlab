function [rho, image, rQCc, proj] = renderUnity(rCNn, Rnc, doGeo)
%renderUnity(rCNn, Rnc, doGeo)
%   rCNn	- Camera position vector
%	Rnc		- Camera rotation matrix 
%	doGeo	- should always be 1. 0 means do GPU depth evaluation. This is
%	not working yet.
	Tnu		= [0,0,1;
			   1,0,0;
			   0,-1,0];
	Tun		= Tnu';	   
	rCNu	= Tun*rCNn;

	Tcr		= [0,0,1;
			   1,0,0;
			   0,-1,0];
	Trc		= Tcr';	   
	
	Rur			= Tun*Rnc*Tcr;
	Qur			= rot2quat(Rur);
	if(nargin ==3)
		[rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur, doGeo);
	else
		[rho, image, rQCr, proj] = setPoseGetFrame(rCNu,Qur);
	end
	if(nargout >= 3)
		rQCc		= Tcr*rQCr;
	end
end

