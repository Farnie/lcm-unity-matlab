% clear java
dynamic_jp	= javaclasspath('-dynamic');
dynamic_n	= length(dynamic_jp);
for i = 1:dynamic_n
	pathi	= dynamic_jp{i};
	javarmpath(pathi)
end

%% Get path of this script
thisfilepath		= mfilename('fullpath');
splitchar			= [filesep];
pathbreak			= strsplit(thisfilepath,splitchar);
npath				= length(pathbreak);
dir					= "";
for i = 2:(npath-1)
	dir				= dir + string(filesep) + pathbreak(i);
end

%% add files to java class
javaaddpath(dir + string(filesep) + "unity.jar");
% Add LCM jar
% Expecting it to be in ${HOME_DIRECTORY}/lcm/build/lcm-java
lcmdir			= string(getuserdir()) + string(filesep) + "lcm";
lcmbuilddir		= lcmdir + string(filesep) + "build";
lcmjardir		= lcmbuilddir + string(filesep) + "lcm-java";

% if(ismac)
% 	lcmjardir = "/usr/local/Cellar/lcm/1.4.0/share/java";
% end

javaaddpath(lcmjardir + string(filesep) + "lcm.jar");

dynamic_jp	= javaclasspath('-dynamic');
fprintf('Dynamic jars: \n')
fprintf('%s %s %d %d %d %3.2f\n',dynamic_jp{:});
fprintf('\n');