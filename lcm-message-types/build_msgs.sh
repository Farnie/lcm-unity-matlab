#!/bin/sh

cwd=$(pwd)
dir_lcm_build="$HOME/git/lcm/build"

dir_src_csharp="../scripts-for-unity/Scripts"
dir_src_java="./unity"
dir_src_python="${cwd}/../lcm-interface/python"
dir_src_c="${cwd}/../lcm-interface/c/unity"

path_unity_jar="${cwd}/../lcm-java/unity.jar"
path_lcm_jar="${dir_lcm_build}/lcm-java/lcm.jar"



## ----------------------- JAVA -----------------------
files="*.lcm"
echo "Generating java files: $files"
lcm-gen -j $files

files="${dir_src_java}/*.java"
echo " Compiling java files: $files"
echo "         with lcm.jar: $path_lcm_jar"

javac -classpath $path_lcm_jar $files


# # ls ${JAVA_SRC}/*.class
jar cf "$path_unity_jar" ${dir_src_java}/*.class

## ----------------------- C# [Unity] -----------------------

lcm-gen  --csharp --csharp-path "$dir_src_csharp" *.lcm 


# ## ----------------------- C -----------------------

mkdir -p "$dir_src_c"
lcm-gen --c  --c-cpath "$dir_src_c"  --c-hpath "$dir_src_c" *.lcm

# ## ----------------------- PYTHON -----------------------

lcm-gen --python --ppath  "$dir_src_python" *.lcm

