function Kc = generateKc(unityproj, nu, nv)
%GENERATEK Summary of this function goes here
%   Detailed explanation goes here
	Ric				= [0,1,0;0,0,1;1,0,0];
	
	Ks   			= blkdiag(unityproj(1:2,1:2), 1);
	Tmis			= diag([(nu)/2, (nv)/2, 1]);
	Tmis(1:2, 3)	= [(nu+1)/2, (nv+1)/2];
	Kc				= Tmis*Ks;
end

