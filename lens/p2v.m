function [rQCc, dudp] = p2v(p, Kc)
%p2v is a pixel to vector mapping for a planar camera model parameterised
%by Kc. 
%   Kc is the  planar camera model described in the camera basis {c}
%	
	pb				= p;
	pb(3,:)			= 1;
	rPCc			= Kc\pb;
	if(nargout==1)
		rQCc	= uvec(rPCc);
	elseif(nargout==2)
		nr				= size(p, 2);
		[rQCc, durdr]	= uvec(rPCc);
		dpbdp			= [eye(2); zeros(1,2)];
        
        % Calculate 
% 		drdpb			= inv(Kc);
%		drdp			= drdpb*dpbdp;
		drdp			= Kc\dpbdp;
        
% 		dudp			= zeros(3, 2, nr);
% 		parfor i = 1:nr
% 			dudpi		= durdr(:,:,i)*drdp;
% 			dudp(:,:,i) = dudpi;
%         end
        
        
        dudp   = mm3d(durdr, drdp);
	end
end

