function [ur,durdr] = uvec(r)
%UVEC Create unit vectors for each column vector in r

	ur			= normc(r);
	[nr, nc]	= size(r);
	
	% Generate unit vector gradients if specified
	if(nargout==2)
		durdr	= zeros(nr, nr, nc);
		norms	= sqrt(sum(r.^2,1));
		for i = 1:nr
			ei				= zeros(nr,1);ei(i) = 1;
			durdri			= ei./norms - r.*((norms).^-(3)).*r(i,:);
			durdr(:,i,:)	= reshape(durdri, nr, 1, nc);
		end
	end
end
