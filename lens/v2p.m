function [p, dpdr] = v2p(rQCc, Kc)
%V2P is a vector to pixel mapping for a planar camera model parameterised
%by Kc. 
%	
	pb		= Kc*rQCc;
	p		= pb(1:2,:)./pb(3,:);
	
	if(nargout==2)
		nr		= size(rQCc, 2);
		
		v1		= pb(1,:);
		v2		= pb(2,:);
		v3		= pb(3,:);

		dTdpb1	= [1;0]./v3;
		dTdpb2	= [0;1]./v3;
		dTdpb3	= -[v1;v2]./((v3).^2);

		dpbdr	= Kc;
%         dpdr	= zeros(2, 3, nr);
% 		  parfor i = 1:nr
% 			dTdpb			= [dTdpb1(:,i), dTdpb2(:,i), dTdpb3(:,i)];
% 			dpdr(:,:,i)		= dTdpb*dpbdr;
%         end

        dTdpb   = cat(2, reshape(dTdpb1,2, 1, nr), reshape(dTdpb2,2, 1, nr), reshape(dTdpb3,2, 1, nr));
        dpdr    = mm3d(dTdpb, dpbdr);
	end
end

