function [bgr] = draw_hsv(flow)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
[~, h, w] = size(flow);
fx = flow(1,:,:);
fy = flow(2,:,:);
ang = atan2(fy, fx) + pi;
v = sqrt(fx.*fx+fy.*fy);

% Normalize to [0, 1]:
m = min(v);
range = max(v) - m;
v = (v - m) ./ range;

% Then scale to [0,255]:
range2 = 255 - 0;
v = (v*range2) + 0;

hsv = zeros(h, w, 3);
hsv(:,:,1) = ang*(180/pi/2);
hsv(:,:,2) = 255;
hsv(:,:,3) = v;

bgr = hsv2rgb(hsv);
end

