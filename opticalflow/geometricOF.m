function [dBrQBc]  = geometricOF(vCNc, omegaCNc, rho, rQCc)
    
   
    
    nt                  = size(vCNc,2);
    ny                  = size(rQCc, 2);
    
    idx                 = repelem(1:nt,ny);
    vCNc                = vCNc(:,idx);
    omegaCNc            = omegaCNc(:,idx);
    rQCc                = repmat(rQCc, 1, nt);
    invdepth            = rho(:)';
	% Calculate optical flow
    dBrQBc              = cross(rQCc, cross(rQCc, invdepth.*vCNc) + omegaCNc);
    dBrQBc              = reshape(dBrQBc, 3, ny, nt);
end
