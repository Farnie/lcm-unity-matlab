function [dBrQCc] = im2sphereflow(pix, dpdt, param)
%IM2SPHEREFLOW Summary of this function goes here
%   Detailed explanation goes here
	p2v				= param.p2v;
	[~, drdp]		= p2v(pix);
	nr				= size(pix,2);
% 	dBrQCc			= zeros(3,nr);
    dBrQCc          = reshape(mm3d(drdp, reshape(dpdt,2,1,[])), 3, []);
% 	parfor i = 1:nrdrdp
% 		dBrQCci		= drdp(:,:,i)*dpdt(:,i);
% 		dBrQCc(:,i) = dBrQCci;
% 	end

end

