function imflow = sphere2imflow(dBrQCc, param)
%SPHERE2IMFLOW Summary of this function goes here
%   Detailed explanation goes here
	v2p		= param.v2p;
	rQCc	= param.rQCc;
	
	% Move along tangent space of rQCc and then project back to the view
	% sphere
	nr			= size(rQCc,2);
% 	imflow		= zeros(2, nr);
	[~, dpdr]	= v2p(rQCc);
    
    dBrQCc_pages    = reshape(dBrQCc,3,1,nr);
    imflow          = reshape(mm3d(dpdr, dBrQCc_pages), 2, []);
% 	parfor i = 1:nr
% 		drdt			= BdrQCc(:,i);
% 		dpdt			= dpdr(:,:,i)*drdt;
% 		imflow(:,i)		= dpdt;
% 	end

end

