using UnityEngine;
using System;
using System.IO;
using System.Threading.Tasks;


public class Depth
{

    private Shader _shader;
    private Shader shader
    {
        get { return _shader != null ? _shader : (_shader = Shader.Find("Custom/RenderDepth")); }
    }

    private Material _material;
    private Material material
    {
        get
        {
            if (_material == null)
            {
                _material              = new Material(shader);
                _material.hideFlags    = HideFlags.HideAndDontSave;
            }
            return _material;
        }
    }
    
    private Int32 nv, nu, nrays;

    private Texture2D depthTexture;
    private Int32 nbytes_depthTex, nbytes_depth;
    private byte[] textureData;
    private float [] invz;
    private byte [] invz_bytes;

    private Vector3[] rQCr; 
    private float[] rQCr1, rQCr2, rQCr3;
    private byte [] rQCr1_bytes, rQCr2_bytes, rQCr3_bytes;

    // turn on depth rendering for the camera so that the shader can access it via _CameraDepthTexture
        
    public Depth(){
        

    }

    public Int32 init(Camera c, Int32 _nv, Int32 _nu){
        nu                  = _nv;
        nv                  = _nu;
        nrays               = nv*nu;
        nbytes_depth        = nrays * sizeof(float);
        nbytes_depthTex     = nbytes_depth * 4;

        depthTexture        = new Texture2D(nu, nv, TextureFormat.RGBAFloat, false);
        textureData         = new byte[nbytes_depthTex];
        invz                = new float[nrays];
        invz_bytes          = new byte[nbytes_depth];

        rQCr1               = new float[nrays];
        rQCr2               = new float[nrays];
        rQCr3               = new float[nrays];
        
        if (!SystemInfo.supportsImageEffects)
        {
            UnityEngine.Debug.Log("System doesn't support image effects");
            // enabled = false;
            return -1;
        }
        if (shader == null || !shader.isSupported)
        {
            // enabled = false;
            UnityEngine.Debug.Log("Shader " + shader.name + " is not supported");
            return -1;
        }

        
        c.depthTextureMode = DepthTextureMode.Depth;
        createDirectionVectors(c);
        return 0;
    }



    // ----------------------------------- SHADER -----------------------------------
    public Shader getShader(){
        return shader;
    }

    public Material getMaterial(){
        return material;
    }

    public void destroyMaterial(){
        // if (_material != null)
        // DestroyImmediate(_material); 
    }


    // ----------------------------------- GPU InvZ -----------------------------------

    public float [] renderToInvZ(){
        RenderToTexture();
        Texture2D tex = getDepthTexture();
        return shaderToInvZ(tex);
    }

    public byte [] renderToInvZ_byte(){
        RenderToTexture();
        Texture2D tex = getDepthTexture();
        return shaderToInvZ_bytes(tex);
    }

    public float [] getInvZFromTextureSelf(){
        return shaderToInvZ(depthTexture);
    }

    public byte [] getInvZFromTextureSelf_byte(){
        return shaderToInvZ_bytes(depthTexture);
    }

    private void getTextureData(Texture2D image){
        byte[] textureptr       = image.GetRawTextureData();
        Array.Copy(textureptr, 0, textureData, 0, nbytes_depthTex);
    }

    private float [] shaderToInvZ(Texture2D image){

        getTextureData(image);

        float fb0,fb1,fb2,fb3;
        

        Int32 nch               = 4;

        for(Int32 i = 0; i < nrays; i++){
            Int32 idx_cch0      = (nch * sizeof(float)) * i + sizeof(float)*0;
            Int32 idx_cch1      = idx_cch0                  + sizeof(float)*1;
            Int32 idx_cch2      = idx_cch0                  + sizeof(float)*2;
            Int32 idx_cch3      = idx_cch0                  + sizeof(float)*3;

            fb0                 = System.BitConverter.ToSingle(textureData, idx_cch0)*255;
            fb1                 = System.BitConverter.ToSingle(textureData, idx_cch1)*255;
            fb2                 = System.BitConverter.ToSingle(textureData, idx_cch2)*255;
            fb3                 = System.BitConverter.ToSingle(textureData, idx_cch3)*255;

            byte[] word         = new byte[4];
            word[0]             = ((byte)fb0);
            word[1]             = ((byte)fb1);
            word[2]             = ((byte)fb2);
            word[3]             = ((byte)fb3);

            invz[i]             = System.BitConverter.ToSingle(word, 0);
        
        }

        return invz;
    }

    public byte [] shaderToInvZ_bytes(Texture2D image){
        getTextureData(image);
        float fb0;
        
        for(Int32 i = 0; i < nbytes_depth; i++){
            Int32 idx_cch0      = (sizeof(float)) * i;
            fb0                 = System.BitConverter.ToSingle(textureData, idx_cch0)*255;

            invz_bytes[i]        = (byte) fb0;
        }

        return invz_bytes;
    }



    // ----------------------------------- Textures -----------------------------------
    public void RenderToTexture(){

        
        depthTexture.ReadPixels(new Rect(0, 0, nu, nv), 0, 0);
        depthTexture.Apply();

    }

    public Texture2D getDepthTexture(){
        return depthTexture;
    }


    // ----------------------------------- CPU InvDepth -----------------------------------
    public float [] geometricInvDepth(Camera c){

        float[] rho_all     = new float[nrays];
        for (int i = 0; i < nrays; i++) { 
            rho_all[i] = singleCast(c, i);
        }
        return rho_all;
    }

    private float singleCast(Camera c, int idx){
        RaycastHit hit;
        float rho           = 0;

        Quaternion Qur      = c.transform.rotation;
        Vector3 rCNu        = c.transform.position; 

        Vector3 rQCu        = Qur * rQCr[idx];
        Ray ray             = new Ray(rCNu, rQCu);

        bool thereWasAHit   = Physics.Raycast(ray, out hit);
        float range         = hit.distance;

        if (thereWasAHit && range <= c.farClipPlane){

            rho                 = (float)1.0 / range;

            Vector3 rPCu        = hit.distance * ray.direction;

        }
        else
        {
            rho = 0;
        }
        return rho;
    }


    // ----------------------------------- Camera projection -----------------------------------

    public Int32 getWidth(){
        return nu;
    }

     public Int32 getHeight(){
        return nv;
    }

    private void createDirectionVectors(Camera c)
    {

        Quaternion Qur = c.transform.rotation;
        Quaternion Qru = Quaternion.Inverse(Qur);

        rQCr = new Vector3[nrays];

        Vector3 rQCri ;
        float normy, normx;
        int rayIdx = 0;
        float  _v, _u;
        for (int v = 0; v < nv; v++)
        {
            for (int u = 0; u < nu; u++)
            {
                // https://docs.unity3d.com/ScriptReference/Camera.ScreenPointToRay.html
                //  The bottom-left of the screen is (0,0); the right-top is (pixelWidth -1,pixelHeight -1).
                _v              = ((nv-1) - v)  + 0f;
                _u              = u             + 0f;
                Ray ray         = c.ScreenPointToRay(new Vector3(_u, _v, 0));

                rayIdx          = nv * u + v;

                
                rQCri           = Qru * ray.direction;
                rQCr[rayIdx]    = rQCri;
                rQCr1[rayIdx]   = rQCri.x;
                rQCr2[rayIdx]   = rQCri.y;
                rQCr3[rayIdx]   = rQCri.z;
    // debug(rQCc[rayIdx]);
            }
        }
        rQCr1_bytes     = toBytes(rQCr1);
        rQCr2_bytes     = toBytes(rQCr2);
        rQCr3_bytes     = toBytes(rQCr3);
    }
    public Vector3 [] get_rQCr(){
        return rQCr;
    }

    static byte[] toBytes(float[] values)
    {
        var result = new byte[values.Length * sizeof(float)];
        Buffer.BlockCopy(values, 0, result, 0, result.Length);
        return result;
    }

    public byte [] get_rQCr1_bytes(){    
        return rQCr1_bytes;
    }

    public byte [] get_rQCr2_bytes(){    
        return rQCr2_bytes;
    }

    public byte [] get_rQCr3_bytes(){    
        return rQCr3_bytes;
    }


    public float [] getCamProj(Camera c){
        Matrix4x4  camproj      = c.projectionMatrix;
        int p_numel             = 16;
        float[]      p          = new float [p_numel];
        int n_rows, n_cols;
        int idx;

        n_rows  = 4;
        n_cols  = 4;
        for(int i = 0; i<n_rows; i++){
            for(int j = 0; j<n_cols; j++){
                idx         = j*n_rows + i;
                p[idx]      = camproj[i, j];
            }
        }
        return p;
    }
}