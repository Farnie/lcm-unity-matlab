using UnityEngine;
using System;
using System.IO;


// [ExecuteInEditMode]
public class RenderDepth : MonoBehaviour
{
    [Range(0f, 3f)]
    // public float depthLevel = 0.5f;

    

    private Camera camera;
    Int32 nrays, nu, nv;
    private Depth depth     = new Depth();
    

    private void Start ()
    {
        
        nu          = (int)Screen.width;
        nv          = (int)Screen.height;

        nrays       = nu*nv;
        camera      =  Camera.main;
        if(depth.init(camera, nu, nv)!=0){
            return;
        }

    }

    private void OnDisable()
    {
        depth.destroyMaterial();
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (depth.getShader() != null)
        {
            Graphics.Blit(src, dest, depth.getMaterial());
            RenderToTexture();

        }
        else
        {
            Graphics.Blit(src, dest);
        }
    }

    void RenderToTexture()
    {
 
        saveDepth();
        QuitGame();
    }

    // private float [] getCamProj(Camera c){

    private void saveDepth(){
        Int32 w                 = depth.getWidth();
        Int32 h                 = depth.getHeight();
        float near              = camera.nearClipPlane;
        float far               = camera.farClipPlane;

        // Get GPU Depth
        bool useBytes           = true;
        byte[] gpu_invz_byte    = null;
        float[] gpu_invz        = null;
        Int32 nbytes_invz       = 0;
        if(useBytes){
            gpu_invz_byte           = depth.renderToInvZ_byte();
        }else{
            gpu_invz                = depth.renderToInvZ();
        }

        

        // Get CPU inv Depth
        float[] rho             = depth.geometricInvDepth(camera);        

        string  foldername      = w.ToString() + "x" + h.ToString();
        string  path            = Path.Combine("Assets", "gpudepth", foldername);
        Directory.CreateDirectory(path);
        var     fileName        = Path.Combine("Assets", "gpudepth", foldername, "depth_img.bytes");

        float[]   proj          = depth.getCamProj(camera);
        Int32     nproj         = proj.Length; 

        Int32   filesize        = sizeof(Int32);
        filesize                += sizeof(Int32);
        filesize                += sizeof(Int32);
        filesize                += sizeof(float);
        filesize                += sizeof(float);
        filesize                += sizeof(Int32);
        filesize                += sizeof(float)*nproj;

        // GPU depth
        filesize                += sizeof(Int32);
        filesize                += sizeof(float)*nrays;

        // geometric inverse depth
        filesize                += sizeof(Int32);
        filesize                += sizeof(float)*nrays;

        using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
        {
            writer.Write(filesize);
            writer.Write(h);
            writer.Write(w);
            writer.Write(near);
            writer.Write(far);

            writer.Write(nproj);
            for (Int32 i = 0; i<nproj; i++)
                writer.Write(proj[i]);

            writer.Write(nrays);
            if(useBytes){
                for (Int32 i = 0; i < (sizeof(float)*nrays); i++){
                    writer.Write(gpu_invz_byte[i]);
                }
            }else{
                for (Int32 i = 0; i < nrays; i++){
                    writer.Write(gpu_invz[i]);
                }
            }

            writer.Write(nrays);
            for (Int32 i = 0; i<nrays; i++)
                writer.Write(rho[i]);

            writer.Close();
        }
    }
    

    public void QuitGame()
    {
    // save anv game data here
    #if UNITY_EDITOR
    // Application.Quit() does not work in the editor so
    // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif
    }

}