﻿#define  DEBUG_RAYCAST
// #undef  DEBUG_RAYCAST

using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using static System.Environment;

using LCM;
namespace LCM{
 
    public class raycast : MonoBehaviour
    {
        
        // --------------------- RENDER SETUP ---------------------

    
        // --------------------- RENDER SETUP - DONE ---------------------

        private Camera camera;
		private DateTime startTime;

        private Vector3 []  rQCr;
        private int         nrays;
        private int         nmsgs_raw, nmsgs_rho;

        
        private string      lens                    	= "lens";
        private bool        doingRender             	= false;
        private bool        sendLens                	= false;
        private bool        doGeoDepth              	= false;

        // ------------------------- LCM -----------------------------
        private LCM.LCM myLCM;
        SimpleSubscriber subscription   				= new SimpleSubscriber().getSingleton();

        // LCM Message ID
        private int lcm_id 								= 0;
        private int lcm_depthrender_id 				    = -1;
        private int lcm_imagerender_id                  = -1;
        private int lastID              				= 0;
        private int currentID 							= 0;

        // LCM Packet Control
        private static int pkt_initID 					= 0;
        private static int rend_maxPacketLength 		= 40000;
        private static int lens_maxPacketLength  		= 10000;
        

        // LCM Settings
        private static string HOME_DIR                  = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        private static string LCM_TAG_CTL               = Path.Combine(HOME_DIR, "UNITY_CTL");
        private static string LCM_TAG_RENDER_RHO        = Path.Combine(HOME_DIR, "UNITY_RENDER_RHO"); 
        private static string LCM_TAG_RENDER_IMG        = Path.Combine(HOME_DIR, "UNITY_RENDER_IMG");
        private static string LCM_TAG_LENS              = Path.Combine(HOME_DIR, "UNITY_LENS");
        

        Vector3     rCNu;
        Quaternion  Qur;

        byte []     inverseDepth_byte;
        byte []     image_local ;

        private int   nu, nv;
        private bool    isDone          = false;
        private int   nMeasDepth        = 0;
        private int   nframes           = 1;
        private Depth depth             = new Depth();
        private Texture2D   image;

        private void Start()
        {
            tic();
            #if DEBUG_RAYCAST
                debug("[INFO] - Start: Entry");
                debug("[INFO] - Sup bro");
                debug("[INFO] - HOME_DIR: " + HOME_DIR);
            #endif
            nu          = (int)Screen.width;
            nv          = (int)Screen.height;

            image       = new Texture2D(nu, nv, TextureFormat.RGB24, false);

            nrays       = nu*nv;
            camera      =  Camera.main;
            if(depth.init(camera, nu, nv)!=0){
                QuitGame();
            }

            try
            {
                string channel  = "udpm://239.255.76.67:7667?ttl=1";
                myLCM           = new LCM.LCM(channel);
                #if DEBUG_RAYCAST
                    debug("[INFO] - Start: LCM");
                #endif
                #if DEBUG_RAYCAST
                    debug("[INFO] - Start: " + myLCM.ToString());
                #endif
                myLCM.SubscribeAll(subscription);
            }
            catch (Exception ex)
            {
                QuitGame();
            }
            

            rQCr        = depth.get_rQCr();
            QualitySettings.anisotropicFiltering = AnisotropicFiltering.ForceEnable;    
        
            #if DEBUG_RAYCAST
                debug("[INFO] - Start: Exit");
            #endif

        }

       
        void Update()
        {
         
            
            if(subscription.isMsgAvailable()){


                #if DEBUG_RAYCAST
                    debug("Render control recieved");
                #endif
                

                unity.ctl_t  msg            = subscription.getNextMessage();

                print_ctl_msg(msg);
                lcm_id                      = msg.id;
                float []f_rCNu              = msg.rCNu;
                float []f_Thetaur           = msg.Thetaur;

                float w,x,y,z;
                float PI = 3.1415926535897931f;
                x                           = f_rCNu[0];
                y                           = f_rCNu[1];
                z                           = f_rCNu[2];
                rCNu                        = new Vector3(x, y, z);

                // w                           = f_Thetaur[0];
                x                           = f_Thetaur[0] * 180 / PI;
                y                           = f_Thetaur[1] * 180 / PI;
                z                           = f_Thetaur[2] * 180 / PI;
                // Qur                         = new Quaternion(x, y, z, w);

                Quaternion Q_x              = Quaternion.Euler(x,0,0);
                Quaternion Q_y              = Quaternion.Euler(0,y,0);
                Quaternion Q_z              = Quaternion.Euler(0,0,z);
                Qur                         = Q_z * Q_y * Q_x;

                camera.transform.position   = rCNu;
                camera.transform.rotation   = Qur;
                sendLens                    = msg.runRender == 0;
                doingRender                 = msg.runRender == 1;
                doGeoDepth                  = msg.doGeoDepth== 1;
                
            }

            if(sendLens){

                sendLens                    =  false;
                export_rQCr(0);
            }
            
            
        }


        void print_ctl_msg(unity.ctl_t msg){
            #if DEBUG_RAYCAST
                debug("          id = " + msg.id.ToString());
            #endif
            #if DEBUG_RAYCAST
                debug("        rCNu = " + msg.rCNu.ToString());
            #endif
            #if DEBUG_RAYCAST
                debug("     Thetaur = " + msg.Thetaur.ToString());
            #endif
            #if DEBUG_RAYCAST
                debug("   runRender = " + msg.runRender.ToString());
            #endif
            #if DEBUG_RAYCAST
                debug("  doGeoDepth = " + msg.doGeoDepth.ToString());
            #endif
        }

        private void OnDisable()
        {
            depth.destroyMaterial();
        }

        private void OnRenderImage(RenderTexture src, RenderTexture dest)
        {
            if (depth.getShader() != null && !isDoneRenderDepth() && !doGeoDepth)
            {
                // material.SetFloat("_DepthLevel", depthLevel);
                Graphics.Blit(src, dest, depth.getMaterial());
                // renderDepthWithID();
                #if DEBUG_RAYCAST
                    debug("rendered to GPU texture");
                #endif
            }
            else
            {
                Graphics.Blit(src, dest);
                renderImageWithID();
                #if DEBUG_RAYCAST
                    // debug("rendered image");
                #endif
            }

            if(!isDoneRenderDepth()){
                renderDepthWithID();
                #if DEBUG_RAYCAST
                    debug("rendered to CPU memory");
                #endif
            }


        	if(doingRender && isDoneRenderImage() && isDoneRenderDepth()){
                doingRender                 = false;
                // Camera has now moved, save render and get depth
                #if DEBUG_RAYCAST
                    debug("Now calling processAtRender");
                #endif
                processAtRender(pkt_initID);
            }
        }

        void LateUpdate(){
            // Render depth if it hasn't been done for this packet yet.
            #if DEBUG_RAYCAST
                // debug("calling renderDepthWithID");
            #endif
            // renderDepthWithID();
            #if DEBUG_RAYCAST
                // debug("exited renderDepthWithID");
            #endif
        }
        


        private void OnPostRender(){
            
        }

        private bool isDoneRenderDepth(){
            return lcm_depthrender_id == lcm_id;
        }

        private bool isDoneRenderImage(){
            return lcm_imagerender_id == lcm_id;
        }

        private void renderDepthWithID(){
        	if(!isDoneRenderDepth()){
        		// Do Render

                #if DEBUG_RAYCAST
                    debug("doGeoDepth = " + doGeoDepth.ToString());
                #endif

	        	if(doGeoDepth){
                    #if DEBUG_RAYCAST
                        debug("Doing geometricInvDepth");
                    #endif
                    float [] inverseDepth  = depth.geometricInvDepth(camera);
                    #if DEBUG_RAYCAST
                        debug("Done geometricInvDepth");
                    #endif
                    byte [] fw = {0,0,0,0};
                    
                    Int32 j = 0;
                    Int32 nbytes = inverseDepth.Length * sizeof(float);
                    Int32 fw_idx = 0;
                    for(Int32 i = 0; i < nbytes; i++){
                        fw_idx  = i % sizeof(float);
                        if(fw_idx==0){
                            fw  = System.BitConverter.GetBytes(inverseDepth[j]);
                            j   = j+1;
                        }
                        #if DEBUG_RAYCAST
                            // debug("j = " + j.ToString() + ", fw_idx  = " + fw_idx.ToString());
                        #endif
                        inverseDepth_byte[i]    = fw[fw_idx];
                    }
                    #if DEBUG_RAYCAST
                        debug("Converted geometricInvDepth to bytes");
                    #endif

	            }
	            else{
	                #if DEBUG_RAYCAST
                        debug("Doing renderInvDepth");
                    #endif
	                depth.RenderToTexture();
	                inverseDepth_byte           = depth.getInvZFromTextureSelf_byte();//
	            }
	            lcm_depthrender_id 	= lcm_id;
			}
        }

        private void renderImageWithID(){
            if(!isDoneRenderImage()){
                // Do Render
                image_local     = getFrame();
                lcm_imagerender_id  = lcm_id;
            }
        }

        void processAtRender(int pkt_initID){
            
            #if DEBUG_RAYCAST
                debug("[INFO] - processAtRender(): ");
            #endif
            unity.rho_t   rho;
            unity.img_t   img;

            rho   				          = new unity.rho_t();
            img   				          = new unity.img_t();
            
            
            // Depth calculated in LateUpdate();
            #if DEBUG_RAYCAST
                debug("Depth evaluated in OnRenderImage()");
            #endif
            // renderDepthWithID();
            
            
			// nmsgs_rho 					= getNumMessages(nrays, rend_maxPacketLength);

			int nc 						= 3;
            byte[] raw_bytes 			= image_local;

            img.id  				    = lcm_id;
            img.nv                      = nv;
            img.nu  				    = nu;
            img.nc                      = nc;
            img.size                    = nv * nu * nc;
            img.data                    = raw_bytes;
            
            #if DEBUG_RAYCAST
                debug("Posting " + LCM_TAG_RENDER_IMG + " msg ID: "  + img.id);
            #endif
            myLCM.Publish(LCM_TAG_RENDER_IMG, img);
            #if DEBUG_RAYCAST
                debug(LCM_TAG_RENDER_IMG + " posted");
            #endif

            rho.id                      = lcm_id;
            rho.data                    = inverseDepth_byte;
            rho.nrays                   = nv*nu;
            rho.size                    = rho.nrays*sizeof(float);

            // 
            #if DEBUG_RAYCAST
                debug("Posting " + LCM_TAG_RENDER_RHO + " msg ID: "  + rho.id);
            #endif
            myLCM.Publish(LCM_TAG_RENDER_RHO, rho);
            #if DEBUG_RAYCAST
                debug(LCM_TAG_RENDER_RHO + " posted");
            #endif

        }

        private void export_rQCr(int pkt_initID)
        {
           
            float [] proj           = depth.getCamProj(camera);
            int proj_numel          = proj.Length;
            int idx;
            
            // Post things over LCM
            unity.lens_t lens       = new unity.lens_t();
            lens.id                 = lcm_id;
            lens.proj_numel         = proj_numel;
            lens.proj               = proj;

            // lens.nrays              = (int)nrays;
            lens.nv                 = (int)nv;
            lens.nu                 = (int)nu;

            #if DEBUG_RAYCAST
                debug("Posting " + LCM_TAG_LENS);
            #endif
            myLCM.Publish(LCM_TAG_LENS, lens);
            #if DEBUG_RAYCAST
                debug(LCM_TAG_LENS + " posted");
            #endif
           
        }
        
        private void renderImage(){

            image.ReadPixels(new Rect(0, 0, nu, nv), 0, 0, false);
            image.Apply();
        }

        private byte[] getFrame(){
            #if DEBUG_RAYCAST
                debug("entry");
            #endif

            renderImage();
            // var imageData           = image.EncodeToPNG();
            // string name             = string.Format("{0}/render.png", workingDirectory);
            // File.WriteAllBytes(name, imageData);

            #if DEBUG_RAYCAST
                debug("Converting to byte string");
            #endif
            byte[] bytes = texture2raw(image);

            
            #if DEBUG_RAYCAST
                debug("exit");
            #endif
            return bytes;
        }

        // 
        // https://docs.unity3d.com/ScriptReference/Texture2D.GetRawTextureData.html
        // 
        // https://gist.github.com/DashW/74d726293c0d3aeb53f4
        private byte[] texture2raw(Texture2D image_){

            byte[] textureptr 		= image_.GetRawTextureData();
            Int64 nbytes 			= textureptr.Length;
            byte[] textureData 		= new byte[nbytes];
            Array.Copy(textureptr, 0, textureData, 0, nbytes);


            return textureData;
        }


        public void QuitGame()
        {
         // save anv game data here
         #if UNITY_EDITOR
             // Application.Quit() does not work in the editor so
             // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
             UnityEditor.EditorApplication.isPlaying = false;
         #else
             Application.Quit();
         #endif
        }

        private void tic(){
            startTime = DateTime.Now;
        }

        private double toc(){
            DateTime etime = DateTime.Now;
            double elapsedMillisecs = ((TimeSpan)(etime - startTime)).TotalMilliseconds;
            return elapsedMillisecs/1000;
        }

        private void debug(string info){
            StackTrace stackTrace   = new StackTrace();
            string callername       = stackTrace.GetFrame(1).GetMethod().Name;
            UnityEngine.Debug.Log("[DEBUG] - [" + toc().ToString() + "] in " + callername + "(): " + info);
        }
        

        private void error(string info){
            StackTrace stackTrace   = new StackTrace();
            string callername       = stackTrace.GetFrame(1).GetMethod().Name;
            UnityEngine.Debug.Log("[ERROR] - [" + toc().ToString() + "] in " + callername + "(): " + info);
        }




        
        // -------------------------- LCM ---------------------


        internal class SimpleSubscriber : LCM.LCMSubscriber
        {
            public Vector3      rCNu;
            public Quaternion   Qur;
            public int          id;
            public bool msgAvailable = false;
            unity.ctl_t        msg;
            private static SimpleSubscriber instance;

            public SimpleSubscriber getSingleton(){
                if(instance == null){ 
                    instance = new SimpleSubscriber();
                }

                return instance;
            }

            public bool isMsgAvailable(){
                return msgAvailable;   
            }

            public unity.ctl_t getNextMessage(){
                msgAvailable = false;
                return msg;
            }


            public void MessageReceived(LCM.LCM lcm, string channel, LCM.LCMDataInputStream dins)
            {
                // Console.WriteLine("RECV: " + channel);
                

                if (channel == LCM_TAG_CTL)
                {
                    // UnityEngine.Debug.Log("RECV: " + channel);
                    
                    msg                 = new unity.ctl_t(dins);
                    msgAvailable        = true;
                }
                else{
                    // debug("RECV from unknown channel: " + channel);
                }
            }
        }
    }
}
            