// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/RenderDepth"
 {
     Properties
     {
         _MainTex ("Base (RGB)", 2D) = "white" {}
     }
     SubShader
     {
         Pass
         {
             CGPROGRAM
 
             #pragma vertex vert
             #pragma fragment frag
             #pragma target 4.0

             #include "UnityCG.cginc"

             
             uniform sampler2D _MainTex;
             uniform sampler2D _CameraDepthTexture;
             uniform fixed _DepthLevel;
             uniform half4 _MainTex_TexelSize;


             struct input
             {
                 float4 pos : POSITION;
                 float2 uv : TEXCOORD0;
             };
 
             struct output
             {
                 float4 pos : SV_POSITION;
                 float2 uv : TEXCOORD0;
                 float4 scrPos : TEXCOORD1;
             };
            
 
            output vert(input i)
            {
                output o;
                o.pos       = UnityObjectToClipPos(i.pos);
                o.uv        = MultiplyUV(UNITY_MATRIX_TEXTURE0, i.uv);
                 // why do we need this? cause sometimes the image I get is flipped. see: http://docs.unity3d.com/Manual/SL-PlatformDifferences.html
                #if UNITY_UV_STARTS_AT_TOP
                if (_MainTex_TexelSize.y < 0)
                         o.uv.y = 1 - o.uv.y;
                #endif
                o.scrPos    = ComputeScreenPos(o.pos);
                return o;
            }


            inline float invZ(float z){
                float oneon_far     = _ProjectionParams.w;
                float rho           = (_ZBufferParams.x * z + _ZBufferParams.y) * oneon_far;
                return rho;
            }

            

            inline float4 floatToNormFloat4(float f){
                float b0, b1, b2, b3;
                // Binary cast to  uint32_t
                uint u       = asuint(f); 

                // Store data as [0-1]
                b0              = (float) ((u >>  0) & 0xFF) / 255.0;
                b1              = (float) ((u >>  8) & 0xFF) / 255.0;
                b2              = (float) ((u >> 16) & 0xFF) / 255.0;
                b3              = (float) ((u >> 24) & 0xFF) / 255.0;

                return float4(b0, b1, b2, b3);
            }

             
            float4 frag(output o) : SV_Target
            {
                float4 d;
                float depth     = UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, o.uv));

                float iz        = invZ(depth);
                float3 dirvec   = normalize(ObjSpaceViewDir(o.pos));

                // TODO: Convert inverse z depth to inverse depth
                d               = floatToNormFloat4(iz);

                return d;
            }
            
            ENDCG
         }
     } 
 }