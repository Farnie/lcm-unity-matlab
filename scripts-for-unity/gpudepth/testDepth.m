clc
clear


restoredefaultpath
addpath ~/git/lcm-unity-matlab/general/
addpath ~/git/lcm-unity-matlab/lens/
addpath ~/git/lcm-unity-matlab/opticalflow/

%% Load Data
% fdr             = "1980x1280";
fdr             = "640x480";
% fdr             = "120x100";
% fdr             = "4000x2800";
% fdr             = "10x10";

file_depth      = fdr + string(filesep) + "depth_img.bytes";

fh              = fopen(file_depth,'r');

fsize           = fread(fh,1,'int32');
nv              = fread(fh,1,'int32');
nu              = fread(fh,1,'int32');
near            = fread(fh,1,'float');
far             = fread(fh,1,'float');

% Check file size
s               =dir(file_depth);
the_size        =s.bytes;
err             = the_size - fsize;
assert(err == 0,"Incorrect file size");


% Projection matrix
nproj           = fread(fh,1,'int32');
proj            = zeros(4);
proj(:)         = fread(fh, nproj,'float');

Kc				= generateKc(proj, nu, nv);

param.v2p		= @(v)v2p(v, Kc);
param.p2v		= @(p)p2v(p, Kc);

u				= 1:nu;
v				= 1:nv;
[U,V]			= meshgrid(u,v);
p				= [U(:),V(:)]';
param.rQCc		= param.p2v(p);
rQCc            = param.rQCc;

% Get GPU depth
invz_nfloat   	= fread(fh,1,'int32');
rhoch           = fread(fh, invz_nfloat, 'float'); 

rhogpua         = rhoch;


zrho_gpu      	= zeros(nu,nv);
zrho_gpu(:)   	= rhogpua;
zrho_gpu      	= zrho_gpu';
zrho_gpu    	= zrho_gpu(end:-1:1,:);

z               = reshape(1./zrho_gpu, nv,nu);


% assert(any(z<0,'all')==0, 'Incorrect depth');
rQCc1       	= reshape(rQCc(1,:), nv, nu);
rho_gpu         = zrho_gpu.*rQCc1;

% rho_gpu              	= 1./(depth_gput(:));
rOI                     = min(far,100);
rho_gpu(1./rho_gpu>=rOI)= 0;
depth_gpu               = reshape(1./rho_gpu, nv, nu);


% Get CPU inv depth
nf_depth_cpu 	= fread(fh,1,'int32');
rho_cpu         = fread(fh,nf_depth_cpu,'float');
rho_cpu(1./rho_cpu>=rOI)= 0;
depth_cpu       = reshape(1./rho_cpu, nv, nu);

%% Plot data



rPCc_gpu            = rQCc./rho_gpu(:)';
rPCc_cpu            = rQCc./rho_cpu';


figure(1);clf;



subplot(2,2,2);
plot3(rPCc_gpu(1,:), rPCc_gpu(2,:), rPCc_gpu(3,:),'.','displayname','GPU');hold on
plot3(rPCc_cpu(1,:), rPCc_cpu(2,:), rPCc_cpu(3,:),'.','displayname','CPU');
daspect([1,1,1]);
fontsize            = 18;
xlabel('N - [m]','Interpreter','latex','FontSize', fontsize);
ylabel('E - [m]','Interpreter','latex','FontSize', fontsize);
zlabel('D - [m]','Interpreter','latex','FontSize', fontsize);
set(gca,'ydir','reverse');
set(gca,'zdir','reverse');
legend('show')


a_depth_gpu     = subplot(2,2,1);
clims   = [0, rOI];
h1 = imagesc(depth_gpu, clims);
title('GPU Depth');
daspect([1,1,1]);
colorbar

a_depth_cpu     = subplot(2,2,3);
h2 = imagesc(depth_cpu, clims);
title('CPU Depth');
daspect([1,1,1]);
colorbar

linkaxes([a_depth_gpu, a_depth_cpu]);

subplot(2,2,4);
nrays   = numel(depth_cpu);
plot(1:nrays, 1./rho_cpu(:),'o','displayname','CPU'); hold on
plot(1:nrays, 1./rho_gpu(:),'x','displayname','GPU'); 
xlabel('Ray index');
ylabel('Depth - [m]');
legend('show');
xlim([1,nrays]);


